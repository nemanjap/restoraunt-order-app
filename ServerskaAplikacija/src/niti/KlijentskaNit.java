/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niti;

import domain.Korisnik;
import domain.Narudzbina;
import domain.OpstiDomenskiObjekat;
import domain.Sto;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.Kontroler;
import main.Server;
import remote.TransferniObjekat;

/**
 *
 * @author Nemanja
 */
public class KlijentskaNit extends Thread {

    private ObjectInputStream inStream;
    private ObjectOutputStream outStream;
    private Korisnik korisnik;

    public ObjectInputStream getInStream() {
        return inStream;
    }

    public void setInStream(ObjectInputStream inStream) {
        this.inStream = inStream;
    }

    public ObjectOutputStream getOutStream() {
        return outStream;
    }

    public void setOutStream(ObjectOutputStream outStream) {
        this.outStream = outStream;
    }

    /*
     public KlijentskaNit(Socket soket, Korisnik korisnik) {
     this.soket = soket;
     this.korisnik = korisnik;
 
     }*/
    public KlijentskaNit(ObjectInputStream in, ObjectOutputStream out, Korisnik korisnik) {
        this.korisnik = korisnik;
        this.inStream = in;
        this.outStream = out;
        this.korisnik = korisnik;
    }

    @Override
    public void run() {
        boolean pokrenuta = true;
        while (pokrenuta) {
            try {
                //inStream = new ObjectInputStream(soket.getInputStream());
                //outStream = new ObjectOutputStream(soket.getOutputStream());
                System.out.println("Klijentska nit: Nit pokrenuta");
                TransferniObjekat to = (TransferniObjekat) inStream.readObject();
                System.out.println("Klijentska nit: Poruka od klijenta je primljena");
                obradiPoruku(to, outStream);
                System.out.println("Klijentska nit: Poruka je obradjena");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(KlijentskaNit.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                pokrenuta = false;
                System.out.println("Klijentska nit: nit je prekinuta usled prekida veze sa klijentom...");
                System.out.println("Klijentska nit: Deregistrujem nit...");
                Server.ulogovaniKorisnici.remove(this);
                System.out.println("Klijentska nit: nit je deregistrovana...");
                //Logger.getLogger(KlijentskaNit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void obradiPoruku(TransferniObjekat to, ObjectOutputStream out) throws IOException {
        try {
            System.out.println("Klijentska nit: Obradjujem poruku...");
            switch (to.getOperacija()) {
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.VRATI_SVE_STOLOVE:
                    System.out.println("Klijentska Nit: Primljen zahtev za stolove...");
                    ArrayList<OpstiDomenskiObjekat> stolovi = Kontroler.getInstance().vratiSveStolove(to.getParametar());
                    System.out.println("Klijentska nit: Upit za stolove uspeso izvrsen...");
                    to.setOperacija(konstante.Konstante.VRATI_SVE_STOLOVE);
                    to.setRezultat(stolovi);
                    System.out.println("Klijentska nit: Stolovi upakovani...");
                    break;
                /////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.ZAPAMTI_REZERVACIJU:
                    System.out.println("Klijentska nit: Primljen zahtev za cuvanje rezervacije...");
                    Kontroler.getInstance().sacuvajRezervaciju(to.getParametar());
                    break;
                /////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.OBRISI_REZERVACIJU:
                    System.out.println("Klijentska nit: Primljen zahtev za brisanje rezervacije...");
                    Kontroler.getInstance().obrisiRezervaciju(to.getParametar());
                    break;
                //////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.IZMENI_REZERVACIJU:
                    System.out.println("Klijentska nit: Primljen zahtev za izmenu rezervacije...");
                    Kontroler.getInstance().izmeniRezervaciju(to.getParametar());
                    break;
                //////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.VRATI_SVE_ARTIKLE:
                    System.out.println("Klijentska nit: Primljen zahtev za artiklima...");
                    Object artikli = Kontroler.getInstance().vratiSveArtikle(to.getParametar());
                    System.out.println("Klijentska nit: Upit za artikle uspesno izvrsen...");
                    to.setOperacija(konstante.Konstante.VRATI_SVE_ARTIKLE);
                    to.setRezultat(artikli);
                    System.out.println("Klijentska nit: Artikli upakovani...");
                    break;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.POTVRDI_NARUDZBINU:
                    System.out.println("Klijentska nit: Primljen zahtev za potvrdu narudzbine...");

                    if (Kontroler.getInstance().daLiPostojiNarudzbina(to.getParametar())) {
                        System.out.println("Klijentska nit: Narudzbina vec postoji, azuriram stavke");
                        Kontroler.getInstance().azurirajStavkeNarudzbine(to.getParametar());
                        System.out.println("Klijentska nit: Unos uspesno azuriran");
                    } else {
                        System.out.println("Klijentska nit: Narudzbenica ne postoji, zapocinjem cuvanje");
                        Kontroler.getInstance().sacuvajNarudzbinu(to.getParametar());
                        System.out.println("Narudzbina uspesno potvrdjena");
                    }
                    break;
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.VRATI_ID_NARUDZBINE:
                    System.out.println("Klijentska nit: Primljen zahtev za id-em narudzbine...");
                    to.setOperacija(konstante.Konstante.VRATI_ID_NARUDZBINE);
                    Object id = Kontroler.getInstance().vratiIDNarudzbine(to.getParametar());
                    to.setRezultat(id);
                    break;
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                case konstante.Konstante.SACUVAJ_RACUN:
                    System.out.println("Klijentska nit: Primljen zahte za cuvanje racuna...");
                    Kontroler.getInstance().sacuvajRacun(to.getParametar());
                    break;

            }
        } catch (Exception ex) {
            to.setIzuzetak(ex);
        }
        out.writeObject(to);
        System.out.println("Klijentska nit: Poruka uspesno poslata");
    }
}
