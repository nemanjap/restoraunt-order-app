/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import db.DatabaseBroker;
import domain.OpstiDomenskiObjekat;
import java.util.ArrayList;
import sistemske_operacije.artikli.SOVratiSveArtikle;
import sistemske_operacije.login.SOLogin;
import sistemske_operacije.narudzbine.SOAzurirajStavkeNarudzbine;
import sistemske_operacije.narudzbine.SODaLiPostojiNarudzbina;
import sistemske_operacije.narudzbine.SOSacuvajNarudzbinu;
import sistemske_operacije.narudzbine.SOSacuvajRacun;
import sistemske_operacije.narudzbine.SOVratiIdNaudzbine;
import sistemske_operacije.rezervacije.SOIzmeniRezervaciju;
import sistemske_operacije.rezervacije.SOObrisiRezervaciju;
import sistemske_operacije.rezervacije.SOZapamtiRezervaciju;
import sistemske_operacije.stolovi.SOVratiSveStolove;

/**
 *
 * @author Nemanja
 */
public class Kontroler {
    
    private DatabaseBroker dbb;
    private static Kontroler instance;
    
    private Kontroler(){}
    
    public static Kontroler getInstance(){
        if(instance == null)
            instance = new Kontroler();
        return instance;
    }
    
    public OpstiDomenskiObjekat obradiLogin(Object korisnik) throws Exception{
        SOLogin soLogin = new SOLogin();
        soLogin.izvrsiOperaciju(korisnik);
        return soLogin.getObjekat();
    }
    
    public ArrayList<OpstiDomenskiObjekat> vratiSveStolove(Object obj) throws Exception{
      SOVratiSveStolove soVratiStolove = new SOVratiSveStolove();
      soVratiStolove.izvrsiOperaciju(obj);
      return soVratiStolove.getStolovi();
    }
    
    public void sacuvajRezervaciju(Object rezervacija) throws Exception{
        SOZapamtiRezervaciju so = new SOZapamtiRezervaciju();
        so.izvrsiOperaciju(rezervacija);
    }
    
    public void obrisiRezervaciju(Object rezervacija) throws Exception{
        SOObrisiRezervaciju so = new SOObrisiRezervaciju();
        so.izvrsiOperaciju(rezervacija);
    }
    
    public void izmeniRezervaciju(Object rezervacija) throws Exception{
        SOIzmeniRezervaciju so = new SOIzmeniRezervaciju();
        so.izvrsiOperaciju(rezervacija);
    }
    
    public Object vratiSveArtikle(Object obj) throws Exception{
        SOVratiSveArtikle so = new SOVratiSveArtikle();
        so.izvrsiOperaciju(obj);
        return so.getPar();
    }
    
    public Object vratiIDNarudzbine(Object obj) throws Exception{
        SOVratiIdNaudzbine so = new SOVratiIdNaudzbine();
        so.izvrsiOperaciju(obj);
        return so.getId();
    }
    
    public void sacuvajNarudzbinu(Object obj) throws Exception{
        SOSacuvajNarudzbinu so = new SOSacuvajNarudzbinu();
        so.izvrsiOperaciju(obj);
    }
    
    public boolean daLiPostojiNarudzbina(Object obj) throws Exception{
        SODaLiPostojiNarudzbina so = new SODaLiPostojiNarudzbina();
        so.izvrsiOperaciju(obj);
        return so.isPostoji();
    }
    
    public void azurirajStavkeNarudzbine(Object obj) throws Exception{
        SOAzurirajStavkeNarudzbine so = new SOAzurirajStavkeNarudzbine();
        so.izvrsiOperaciju(obj);
    }
    
    public void sacuvajRacun(Object obj) throws Exception{
        SOSacuvajRacun so = new SOSacuvajRacun();
        so.izvrsiOperaciju(obj);
    }
    
}
