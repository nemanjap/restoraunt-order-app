/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.stolovi;

import db.DatabaseBroker;
import domain.OpstiDomenskiObjekat;
import domain.Sto;
import java.util.ArrayList;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOVratiSveStolove extends OpstaSistemskaOperacija {

    private ArrayList<OpstiDomenskiObjekat> stolovi;

    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
        System.out.println("SOVratiStolove: Nema preduslova za operaciju VratiSveStolove");
    }

    public ArrayList<OpstiDomenskiObjekat> getStolovi() {
        return stolovi;
    }


    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        stolovi = DatabaseBroker.getInstance().vratiSveSlozeniObjekat((OpstiDomenskiObjekat)obj);
    }
}
