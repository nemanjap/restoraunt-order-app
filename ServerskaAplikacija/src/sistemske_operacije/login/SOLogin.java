/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.login;

import db.DatabaseBroker;
import domain.Korisnik;
import domain.OpstiDomenskiObjekat;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOLogin extends OpstaSistemskaOperacija{
    private OpstiDomenskiObjekat objekat;
    
    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
        System.out.println("SOLogin: Nema preduslova za login...");
    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        objekat = DatabaseBroker.getInstance().vrati((OpstiDomenskiObjekat) obj);
    }

    public OpstiDomenskiObjekat getObjekat() {
        return objekat;
    }
}
