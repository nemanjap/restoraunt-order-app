/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije;

import db.DatabaseBroker;

/**
 *
 * @author Nemanja
 */
public abstract class OpstaSistemskaOperacija {
    
    public final void izvrsiOperaciju(Object obj) throws Exception {
        try {
            DatabaseBroker.getInstance().ucitajDriver();
            DatabaseBroker.getInstance().uspostaviKonekciju();
            proveriPreduslov(obj);
            izvrsiKonkretnuOperaciju(obj);
            DatabaseBroker.getInstance().commitTransakcije();
        } catch (Exception ex) {
            ex.printStackTrace();
            DatabaseBroker.getInstance().rollbackTransakcije();
            throw ex;
        }
        DatabaseBroker.getInstance().zatvoriKonekciju();
    }
    
    protected abstract void proveriPreduslov(Object obj) throws Exception;
    
    protected abstract void izvrsiKonkretnuOperaciju(Object obj) throws Exception;
    
}
