/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.artikli;

import db.DatabaseBroker;
import domain.Jelo;
import domain.OpstiDomenskiObjekat;
import domain.Pice;
import java.util.ArrayList;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOVratiSvaJela extends OpstaSistemskaOperacija{

    private ArrayList<OpstiDomenskiObjekat> jela;
    
    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
        
    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        jela = DatabaseBroker.getInstance().vratiSve((OpstiDomenskiObjekat)obj);
    }

    public ArrayList<OpstiDomenskiObjekat> getJela() {
        return jela;
    }

}
