/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.artikli;

import domain.Jelo;
import domain.Pice;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOVratiSveArtikle extends OpstaSistemskaOperacija{

    private Object[] par = new Object[2];

    public Object[] getPar() {
        return par;
    }
    
    @Override
    protected void proveriPreduslov(Object obj) throws Exception {

    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        SOVratiSvaJela soJela = new SOVratiSvaJela();
        soJela.izvrsiKonkretnuOperaciju(new Jelo());
        SOVratiSvaPica soPica = new SOVratiSvaPica();
        soPica.izvrsiKonkretnuOperaciju(new Pice());
        par[0] = soJela.getJela();
        par[1] = soPica.getPica();
    }
    
}
