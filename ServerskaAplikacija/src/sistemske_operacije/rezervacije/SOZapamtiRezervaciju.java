/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.rezervacije;

import db.DatabaseBroker;
import domain.OpstiDomenskiObjekat;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOZapamtiRezervaciju extends OpstaSistemskaOperacija{

    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
        System.out.println("SOZapamtiOperaciju: Nema preduslova za operaciju");
    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        DatabaseBroker.getInstance().sacuvaj((OpstiDomenskiObjekat)obj);
    }
    
}
