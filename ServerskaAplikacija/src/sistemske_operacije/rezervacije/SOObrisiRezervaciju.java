/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.rezervacije;

import db.DatabaseBroker;
import domain.OpstiDomenskiObjekat;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOObrisiRezervaciju extends OpstaSistemskaOperacija{

    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
        System.out.println("SOObrisiRezervaciju: Ne postoji preduslov...");
    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        DatabaseBroker.getInstance().obrisi((OpstiDomenskiObjekat)obj);
    }
    
}
