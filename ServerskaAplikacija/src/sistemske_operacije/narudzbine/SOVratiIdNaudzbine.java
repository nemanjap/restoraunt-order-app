/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.narudzbine;

import db.DatabaseBroker;
import domain.OpstiDomenskiObjekat;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOVratiIdNaudzbine extends OpstaSistemskaOperacija{

    long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
        
    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        id = DatabaseBroker.getInstance().vratiID((OpstiDomenskiObjekat)obj);
    }
    
}
