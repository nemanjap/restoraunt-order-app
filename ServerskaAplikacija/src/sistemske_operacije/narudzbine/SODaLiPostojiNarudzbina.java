/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.narudzbine;

import db.DatabaseBroker;
import domain.Narudzbina;
import domain.OpstiDomenskiObjekat;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SODaLiPostojiNarudzbina extends OpstaSistemskaOperacija {

    private boolean postoji;

    @Override
    protected void proveriPreduslov(Object obj) throws Exception {
    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        postoji = DatabaseBroker.getInstance().daLiPostoji((OpstiDomenskiObjekat) obj);
    }

    public boolean isPostoji() {
        return postoji;
    }
}
