/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemske_operacije.narudzbine;

import db.DatabaseBroker;
import domain.Narudzbina;
import domain.OpstiDomenskiObjekat;
import sistemske_operacije.OpstaSistemskaOperacija;

/**
 *
 * @author Nemanja
 */
public class SOSacuvajRacun extends OpstaSistemskaOperacija{

    @Override
    protected void proveriPreduslov(Object obj) throws Exception {

    }

    @Override
    protected void izvrsiKonkretnuOperaciju(Object obj) throws Exception {
        DatabaseBroker.getInstance().sacuvaj((OpstiDomenskiObjekat) obj);
        OpstiDomenskiObjekat odo = (OpstiDomenskiObjekat)obj;
        OpstiDomenskiObjekat odo1 = odo.vratiPovezaniSlozeniObjekat();
        DatabaseBroker.getInstance().postaviLogickuVrednostAtributa(odo1, "izdatRacun", true);
    }
    
}
