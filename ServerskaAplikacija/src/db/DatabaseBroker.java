/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import domain.OpstiDomenskiObjekat;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author Nemanja
 */
public class DatabaseBroker {

    private Connection connection;
    String db_url;
    String db_user_name;
    String db_user_password;
    String db_driver_name;
    private static DatabaseBroker instance;

    public static DatabaseBroker getInstance() throws IOException {
        if (instance == null) {
            instance = new DatabaseBroker();
        }
        return instance;
    }

    private DatabaseBroker() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(konstante.Konstante.TEKUCI_DIREKTORIJUM + File.separator + "config_db.properties"));
        db_url = properties.getProperty("db_url");
        db_user_name = properties.getProperty("db_user_name");
        db_user_password = properties.getProperty("db_user_password");
        db_driver_name = properties.getProperty("db_driver_name");

    }

    public void ucitajDriver() throws ClassNotFoundException {
        Class.forName(db_driver_name);
    }

    public void uspostaviKonekciju() throws SQLException {
        connection = DriverManager.getConnection(db_url, db_user_name, db_user_password);
        connection.setAutoCommit(false);
    }

    public void zatvoriKonekciju() throws SQLException {
        connection.close();
    }

    public void commitTransakcije() throws Exception {
        try {
            connection.commit();
        } catch (SQLException ex) {
            throw new Exception("Greska pri potvrdi transakcije!");
        }
    }

    public void rollbackTransakcije() throws Exception {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            throw new Exception("Greska pri ponistenju transakcije!");
        }
    }

    //INSERT
    public void sacuvaj(OpstiDomenskiObjekat odo) throws SQLException {
        String upit = "INSERT INTO " + odo.vratiImeTabele() + " " + "(" + odo.vratiNaziveKolona() + ") VALUES " + "(" + odo.vratiVredostiZaInsert() + ")";
        System.out.println(upit);
        PreparedStatement statement = connection.prepareStatement(upit);
        statement.executeUpdate();
        statement.close();
    }
    //DELETE

    public void obrisi(OpstiDomenskiObjekat odo) throws SQLException {
        String upit = "DELETE FROM " + odo.vratiImeTabele() + " WHERE " + odo.vratiUslovZaNadjiSlog();
        PreparedStatement statement = connection.prepareStatement(upit);
        statement.executeUpdate();
        statement.close();
    }
    //SELECT ALL Kriterijum

    public ArrayList<OpstiDomenskiObjekat> vratiSve(OpstiDomenskiObjekat odo) throws SQLException, Exception {
        String sql;
        if (odo.vratiKriterijumPretrage() == null) {
            sql = "SELECT " + odo.vratiNaziveKolonaJoin() + " FROM " + odo.vratiNaziveTabelaJoin();
        } else {
            sql = "SELECT * FROM " + odo.vratiNaziveTabelaJoin() + " WHERE " + odo.vratiKriterijumPretrage();
        }
        Statement sqlNaredba = connection.createStatement();
        ResultSet rs = sqlNaredba.executeQuery(sql);
        return odo.vratiListuIzResultSet(rs);
    }
    //SELECT ALL COMPLEX

    public ArrayList<OpstiDomenskiObjekat> vratiSveSlozeniObjekat(OpstiDomenskiObjekat odo) throws SQLException, Exception {

        String upit = "Select " + odo.vratiNaziveKolonaJoin() + " FROM " + odo.vratiNaziveTabelaJoin() + " WHERE " + odo.vratiUslovZaNadjiSlog();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(upit);

        ArrayList<OpstiDomenskiObjekat> jakObjekat = odo.vratiListuIzResultSet(rs);

        for (OpstiDomenskiObjekat opstiDomenskiObjekat : jakObjekat) {
            if (opstiDomenskiObjekat.vratiPovezaniSlozeniObjekat() != null) {
                OpstiDomenskiObjekat o1 = opstiDomenskiObjekat.vratiPovezaniSlozeniObjekat();
                setujSlabeObjekte(o1);
            }
        }

        for (OpstiDomenskiObjekat opstiDomenskiObjekat : jakObjekat) {
            String upit1 = "Select * From " + odo.vratiJoinTabeleSlabogObjekta() + " Where " + opstiDomenskiObjekat.vratiUslovZaPronalazakSlabogObjekta();
            Statement statement1 = connection.createStatement();
            ResultSet rs1 = statement1.executeQuery(upit1);
            odo.setujPronadjeneSlabeObjekte(rs1, opstiDomenskiObjekat);
        }
        rs.close();
        statement.close();
        return jakObjekat;
    }
    //SELECT CHILD OBJECTS OF A COMPLEX OBJECT

    public void setujSlabeObjekte(OpstiDomenskiObjekat odo) throws SQLException, Exception {
        String upit = "Select * From " + odo.vratiJoinTabeleSlabogObjekta() + " Where " + odo.vratiUslovZaPronalazakSlabogObjekta();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(upit);
        odo.setujPronadjeneSlabeObjekte(rs, odo);
    }
    //UPDATE

    public void izmeni(OpstiDomenskiObjekat odo) throws Exception {
        String sql = "UPDATE " + odo.vratiImeTabele()
                + " SET " + odo.postaviVrednostAtributa()
                + " WHERE " + odo.vratiUslovZaNadjiSlog();
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }
    //INSERT COMPLEX

    public void sacuvajSlozeni(OpstiDomenskiObjekat odo) throws SQLException {
        String upit = "INSERT INTO " + odo.vratiImeTabele() + " VALUES (" + odo.vratiVredostiZaInsert() + ")";
        Statement statement = connection.createStatement();
        statement.execute(upit);

        for (int i = 0; i < odo.vratiBrojVezanihObjekata(); i++) {
            OpstiDomenskiObjekat vezo = odo.vratiSlogVezanogObjekta(i);

            upit = "INSERT INTO " + vezo.vratiImeTabele() + " VALUES (" + vezo.vratiVredostiZaInsert() + ")";
            statement.executeUpdate(upit);

        }
        statement.close();
    }
    //vracanje ID

    public long vratiID(OpstiDomenskiObjekat odo) throws SQLException {
        String upit = "SELECT MAX(" + odo.vratiNazivIdentifikatora() + ") AS maksimum FROM " + odo.vratiImeTabele();
        PreparedStatement statement = connection.prepareStatement(upit);
        ResultSet rs = statement.executeQuery();
        long maksimum = 0;
        if (rs.next()) {
            maksimum = rs.getLong("maksimum");
        } else {
            maksimum = 0;
        }
        System.out.println(maksimum);
        return maksimum;
    }
    //vraca bilo koji objekat zadat kao parametar

    public OpstiDomenskiObjekat vrati(OpstiDomenskiObjekat odo) throws SQLException, Exception {
        OpstiDomenskiObjekat objekat = null;
        String upit = "Select * From " + odo.vratiNaziveTabelaJoin() + " Where " + odo.vratiUslovZaNadjiSlog();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(upit);
        ArrayList<OpstiDomenskiObjekat> lista = odo.vratiListuIzResultSet(rs);
        if (!lista.isEmpty()) {
            objekat = lista.get(0);
        }
        rs.close();
        statement.close();
        return objekat;
    }
    //Provera postojanja odredjenog objekta

    public boolean daLiPostoji(OpstiDomenskiObjekat odo) throws SQLException {
        String upit = "Select count(" + odo.vratiNazivIdentifikatora() + ") as rezultat FROM " + odo.vratiImeTabele() + " Where " + odo.vratiKriterijumPretrage() + " Group by " + odo.vratiNazivIdentifikatora();
        System.out.println(upit);
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(upit);
        int postoji = 0;
        while (rs.next()) {
            postoji = rs.getInt("rezultat");
            break;
        }
        rs.close();
        statement.close();
        if (postoji == 1) {
            return true;
        }
        return false;
    }
    //brise sve slabe obekte povezane sa datim jakim, u slucaju Narudzbine npr. brisu se sve njene stavke

    public void obrisiPovezaneObjekte(OpstiDomenskiObjekat odo) throws SQLException {
        System.out.println(odo.vratiImeTabeleSlabogObjekta());
        System.out.println(odo.vratiUslovZaPronalazakSlabogObjekta());
        String upit = "Delete From " + odo.vratiImeTabeleSlabogObjekta() + " Where " + odo.vratiUslovZaPronalazakSlabogObjekta();
        Statement statement = connection.createStatement();
        statement.executeUpdate(upit);
        statement.close();
    }
    //azurira sve slabe objekte koji su povezani sa datim jakim, u slucaju Narudzbine npr. azuriraju se sve stavke

    public void azurirajStavkeJakogObjekta(OpstiDomenskiObjekat odo) throws SQLException {
        obrisiPovezaneObjekte(odo);
        for (int i = 0; i < odo.vratiBrojVezanihObjekata(); i++) {
            OpstiDomenskiObjekat vezo = odo.vratiSlogVezanogObjekta(i);
            System.out.println(odo);
            String upit = "INSERT INTO " + vezo.vratiImeTabele() + " VALUES (" + vezo.vratiVredostiZaInsert() + ")";
            System.out.println(upit);
            Statement statement = connection.createStatement();
            statement.executeUpdate(upit);
            statement.close();
        }
    }

    //postavlja logicku (boolean) vrednost nekog atributa tabele i polja koje se navode u metodi
    public void postaviLogickuVrednostAtributa(OpstiDomenskiObjekat odo, String nazivPolja, boolean vrednostPolja) throws SQLException {
        String upit = "Update " + odo.vratiImeTabele() + " Set " + nazivPolja + "=" + vrednostPolja + " WHERE " + odo.vratiKriterijumPretrage();
        Statement statement = connection.createStatement();
        statement.execute(upit);
        statement.close();
    }
}
