/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import domain.Korisnik;
import domain.OpstiDomenskiObjekat;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.Kontroler;
import niti.KlijentskaNit;
import remote.TransferniObjekat;

/**
 *
 * @author Nemanja
 */
public class Server {

    public static ArrayList<KlijentskaNit> ulogovaniKorisnici = new ArrayList<>();
    
    public static void main(String[] args) {
        try {
            ServerSocket welcomeSocket = new ServerSocket(9000);
            System.out.println("Server: Server je pokrenut i ceka na klijenta...");
            boolean kraj = false;
            while (!kraj) {
                try {
                    System.out.println("Here I go again");
                    Socket communicationSocket = welcomeSocket.accept();
                    System.out.println("Server: Klijent je uspostavio komunikaciju sa serverom...");
                    System.out.println("Server: Kreiram input i output stream za komunikaciju sa klijentom...");
                    ObjectInputStream in = new ObjectInputStream(communicationSocket.getInputStream());
                    System.out.println("Server: Input stream kreiran...");
                    ObjectOutputStream out = new ObjectOutputStream(communicationSocket.getOutputStream());
                    System.out.println("Server: Output stream kreiran...");
                    System.out.println("Server: Cekam da klijent posalje objekat...");
                    TransferniObjekat to = (TransferniObjekat) in.readObject();
                    System.out.println("Server: Primio objekat od klijenta...");
                    Korisnik k = obradiLogin(to, out);
                    if(k == null){
                        System.out.println("Server: Obradjen login korisnika. Neuspesno logovanje...");
                        continue;
                    }
                    System.out.println("Server: Obradjen login korisnika. Uspesno logovanje...");
                    KlijentskaNit nit = new KlijentskaNit(in, out, k);
                    ulogovaniKorisnici.add(nit);
                    System.out.println("Server: Klijentska nit registrovana...");
                    nit.start();
                    System.out.println("Server: Klijentska nit pokrenuta...");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Korisnik obradiLogin(TransferniObjekat to, ObjectOutputStream out) throws Exception {
        switch (to.getOperacija()) {
            case konstante.Konstante.LOGIN:
                try {
                    OpstiDomenskiObjekat korisnik = Kontroler.getInstance().obradiLogin(to.getParametar());
                    to.setOperacija(konstante.Konstante.LOGIN);
                    to.setRezultat(korisnik);
                    System.out.println(korisnik);
                } catch (Exception ex) {
                    to.setIzuzetak(ex);
                }
                to.setParametar(null);
                out.writeObject(to);
                return (Korisnik)to.getRezultat();
            default:
                throw new Exception("Ne postoji takva operacija");
        }
    }
}
