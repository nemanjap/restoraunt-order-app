/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package konstante;

/**
 *
 * @author Nemanja
 */
public class Konstante {
    
    public static final String TEKUCI_DIREKTORIJUM=System.getProperty("user.dir");
    
    public static final int LOGIN = 1;
    public static final int VRATI_SVE_STOLOVE = 2;
    public static final int ZAPAMTI_REZERVACIJU = 3;
    public static final int OBRISI_REZERVACIJU = 4;
    public static final int IZMENI_REZERVACIJU = 5;
    public static final int VRATI_SVE_ARTIKLE = 6;
    public static final int POTVRDI_NARUDZBINU = 7;
    public static final int VRATI_ID_NARUDZBINE = 8;
    public static final int KONOBAR_REFRESH = 9;
    public static final int SACUVAJ_RACUN = 10;
}
