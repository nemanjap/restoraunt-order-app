/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package remote;

import java.io.Serializable;

/**
 *
 * @author Nemanja
 */
public class TransferniObjekat implements Serializable{
    
    private int operacija;
    private Object parametar;
    private Object rezultat;
    private Object izuzetak;

    public TransferniObjekat() {
    }

    public TransferniObjekat(int operacija, Object parametar) {
        this.operacija = operacija;
        this.parametar = parametar;
    }

    public TransferniObjekat(int operacija, Object rezultat, Object izuzetak) {
        this.operacija = operacija;
        this.parametar = parametar;
        this.rezultat = rezultat;
        this.izuzetak = izuzetak;
    }

    public TransferniObjekat(int operacija, Object parametar, Object rezultat, Object izuzetak) {
        this.operacija = operacija;
        this.parametar = parametar;
        this.rezultat = rezultat;
        this.izuzetak = izuzetak;
    }

    public int getOperacija() {
        return operacija;
    }

    public void setOperacija(int operacija) {
        this.operacija = operacija;
    }

    public Object getParametar() {
        return parametar;
    }

    public void setParametar(Object parametar) {
        this.parametar = parametar;
    }

    public Object getRezultat() {
        return rezultat;
    }

    public void setRezultat(Object rezultat) {
        this.rezultat = rezultat;
    }

    public Object getIzuzetak() {
        return izuzetak;
    }

    public void setIzuzetak(Object izuzetak) {
        this.izuzetak = izuzetak;
    }
    
    
    
    
}
