/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nemanja
 */
public class Korisnik implements Serializable, OpstiDomenskiObjekat {

    private long korisnikId;
    private String ime;
    private String prezime;
    private String username;
    private String password;
    private TipUloge uloga;

    public Korisnik() {
        setKorisnikId(Long.MIN_VALUE);
        setIme("Nepoznato");
        setPrezime("Nepoznato");
        setUsername("Nepoznat");
        setPassword("Nepoznata");
        setUloga(new TipUloge());
    }

    public Korisnik(long korisnikId, String ime, String prezime, String username, String password, TipUloge uloga) {
        setKorisnikId(korisnikId);
        setIme(ime);
        setPrezime(prezime);
        setUsername(username);
        setPassword(password);
        setUloga(uloga);
    }

    public Korisnik(String ime, String prezime, String username, String password, TipUloge uloga) {
        setIme(ime);
        setPrezime(prezime);
        setUsername(username);
        setPassword(password);
        setUloga(uloga);
    }

    public Korisnik(long korisnikId, String ime, String prezime, String username, String password, long sifraUloge, String nazivUloge) {
        setKorisnikId(korisnikId);
        setIme(ime);
        setPrezime(prezime);
        setUsername(username);
        setPassword(password);
        setUloga(new TipUloge(sifraUloge, nazivUloge));
    }

    public Korisnik(String username, String password) {
        setUsername(username);
        setPassword(password);
    }

    public long getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(long korisnikId) {
        this.korisnikId = korisnikId;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TipUloge getUloga() {
        return uloga;
    }

    public void setUloga(TipUloge uloga) {
        this.uloga = uloga;
    }

    @Override
    public String toString() {
        return ime + ", " + prezime + ", " + uloga.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final Korisnik other = (Korisnik) o;
        if (this.korisnikId != other.korisnikId || !this.ime.equals(other.ime) || !this.prezime.equals(other.prezime)) {
            return false;
        }
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "Korisnik";
    }

    @Override
    public String vratiNaziveKolona() {
        return "ime, prezime, username, password, sifraUloge";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return "'" + ime + "', '" + prezime + "', '" + username + "', '" + password + "', " + uloga.getSifraUloge();
    }

    @Override
    public String vratiVredostiZaInsert() {
        return "'" + ime + "', '" + prezime + "', '" + username + "', '" + password + "', " + uloga.getSifraUloge();
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "KorisnikID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(korisnikId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        return "(Korisnik INNER JOIN TipUloge ON (Korisnik.SifraUloge=TipUloge.SifraUloge))";
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) {
        ArrayList<OpstiDomenskiObjekat> lista = new ArrayList<>();
        try {
            while (rs.next()) {
                Korisnik k = new Korisnik();
                k.setKorisnikId(rs.getLong("KorisnikID"));
                k.setIme(rs.getString("Ime"));
                k.setPrezime(rs.getString("Prezime"));
                k.setUloga(new TipUloge(rs.getLong("SifraUloge"), rs.getString("NazivUloge")));
                lista.add(k);
                System.out.println("Dodao korisnika "+k+"U listu.............................................................................");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @Override
    public String postaviVrednostAtributa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        return "Ime='" + username + "' AND password='" + password+"'";
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
