/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Nemanja
 */
public class Sto implements Serializable, OpstiDomenskiObjekat {

    private long stoId;
    private int brojMesta;
    private ArrayList<Rezervacija> rezervacije;
    private Narudzbina narudzbina;

    public Sto() {
        setStoId(Long.MIN_VALUE);
        setBrojMesta(Integer.MIN_VALUE);
        rezervacije = new ArrayList<>();
        narudzbina = new Narudzbina();
    }

    public Sto(long stoId, int brojMesta) {
        setStoId(stoId);
        setBrojMesta(brojMesta);
        rezervacije = new ArrayList<>();
        narudzbina = new Narudzbina();
    }

    public Sto(long stoId, int brojMesta, ArrayList<Rezervacija> rezervacije) {
        setStoId(stoId);
        setBrojMesta(brojMesta);
        setRezervacije(rezervacije);
        narudzbina = new Narudzbina();
    }

    public Sto(long stoId, int brojMesta, ArrayList<Rezervacija> rezervacije, Narudzbina narudzbina) {
        this.stoId = stoId;
        this.brojMesta = brojMesta;
        this.rezervacije = rezervacije;
        this.narudzbina = narudzbina;
    }

    public long getStoId() {
        return stoId;
    }

    public void setStoId(long stoId) {
        this.stoId = stoId;
    }

    public int getBrojMesta() {
        return brojMesta;
    }

    public void setBrojMesta(int brojMesta) {
        this.brojMesta = brojMesta;
    }

    public ArrayList<Rezervacija> getRezervacije() {
        return rezervacije;
    }

    public void setRezervacije(ArrayList<Rezervacija> rezervacije) {
        this.rezervacije = rezervacije;
    }

    public Narudzbina getNarudzbina() {
        return narudzbina;
    }

    public void setNarudzbina(Narudzbina narudzbina) {
        this.narudzbina = narudzbina;
    }

    @Override
    public String toString() {
        return "Sto broj: " + stoId + ",  Broj mesta: " + brojMesta;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final Sto other = (Sto) o;
        if (this.stoId != other.stoId || this.brojMesta != other.brojMesta) {
            return false;
        }
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "Sto";
    }

    @Override
    public String vratiNaziveKolona() {
        return "brojMesta";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return Long.toString(brojMesta);
    }

    @Override
    public String vratiVredostiZaInsert() {
        return Long.toString(brojMesta);
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "StoID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(stoId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        return "(Sto left join (select * from narudzbina where DATEADD(\'d\', 0, DATEDIFF(\'d\', 0,datum)) = DATEADD(\'d\', 0, DATEDIFF(\'d\', 0, now())) and izdatRacun=false) t2 on sto.stoid=t2.stoid) left join konobar on konobar.konobarid=t2.konobarid";
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws SQLException {
        ArrayList<OpstiDomenskiObjekat> stolovi = new ArrayList<>();
        while (rs.next()) {
            long stoId = rs.getLong("StoID");
            int brojMesta = rs.getInt("BrojMesta");
            Sto sto = new Sto(stoId, brojMesta);
            long narudzbinaID = rs.getLong("NarudzbinaId");
            if (narudzbinaID != 0) {
                Date datum = rs.getTimestamp("Datum");
                int popust = rs.getInt("Popust");
                double ukupanIznos = rs.getDouble("UkupanIznos");
                boolean potvrdjena = rs.getBoolean("Potvrdjena");
                boolean spremna = rs.getBoolean("Spremna");
                boolean izdatRacun = rs.getBoolean("IzdatRacun");
                long konobarId = rs.getLong("KonobarId");
                String ime = rs.getString("Ime");
                String prezime = rs.getString("Prezime");
                Konobar konobar = new Konobar(konobarId, ime, prezime);
                Narudzbina narudzbina1 = new Narudzbina(narudzbinaID, datum, popust, ukupanIznos, sto, konobar, null, potvrdjena, spremna, izdatRacun);
                sto.setNarudzbina(narudzbina1);
            } else {
                sto.setNarudzbina(new Narudzbina());
            }
            stolovi.add(sto);
        }
        return stolovi;
    }

    @Override
    public String postaviVrednostAtributa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        return "1=1";
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        return "Rezervacija INNER JOIN Sto ON Rezervacija.StoID=Sto.StoID";
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) throws SQLException {

        ArrayList<Rezervacija> listaRezervacija = new ArrayList<>();
        for (OpstiDomenskiObjekat odo1 : new Rezervacija().vratiListuIzResultSet(rs)) {
            listaRezervacija.add((Rezervacija) odo1);
        }
        ((Sto) odo).setRezervacije(listaRezervacija);
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        return "Sto.StoID=" + stoId + " AND Datum >= date()";
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        return "Sto.StoID as stoID, brojMesta, narudzbinaID, Datum, popust, ukupanIznos, potvrdjena, spremna, izdatRacun, Konobar.KonobarId, Ime, Prezime";
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        return narudzbina;
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        return "Rezervacija";
    }
}
