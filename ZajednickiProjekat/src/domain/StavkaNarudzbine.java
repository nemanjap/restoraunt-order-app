/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Nemanja
 */
public class StavkaNarudzbine implements Serializable, OpstiDomenskiObjekat{
    
    private long rbStavke;
    private int kolicina;
    private double ukupnaVrednost;
    private Artikal artikal;
    private long narudzbinaId;


    public StavkaNarudzbine() {
        setRbStavke(Long.MIN_VALUE);
        setKolicina(0000000);
        setUkupnaVrednost(0000000);
        setArtikal(new Artikal());
    }

    public StavkaNarudzbine(long rbStavke, int kolicina, double ukupnaVrednost, Artikal artikal) {
        setRbStavke(rbStavke);
        setKolicina(kolicina);
        setUkupnaVrednost(ukupnaVrednost);
        setArtikal(artikal);
    }
    
      public StavkaNarudzbine(long rbStavke, int kolicina, double ukupnaVrednost, Artikal artikal, long narudzbinaId) {
        setRbStavke(rbStavke);
        setKolicina(kolicina);
        setUkupnaVrednost(ukupnaVrednost);
        setArtikal(artikal);
        setNarudzbinaId(narudzbinaId);
    }

    public long getRbStavke() {
        return rbStavke;
    }

    public void setRbStavke(long rbStavke) {
        this.rbStavke = rbStavke;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public double getUkupnaVrednost() {
        return ukupnaVrednost;
    }

    public void setUkupnaVrednost(double ukupnaVrednost) {
        this.ukupnaVrednost = ukupnaVrednost;
    }

    public Artikal getArtikal() {
        return artikal;
    }

    public void setArtikal(Artikal artikal) {
        this.artikal = artikal;
    }
    
    
    public long getNarudzbinaId() {
        return narudzbinaId;
    }

    public void setNarudzbinaId(long narudzbinaId) {
        this.narudzbinaId = narudzbinaId;
    }

    @Override
    public String toString() {
        return narudzbinaId+" "+rbStavke+", "+artikal.getNaziv()+", "+kolicina+", "+ukupnaVrednost;
    }

    @Override
    public boolean equals(Object o) {
         if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        final StavkaNarudzbine other = (StavkaNarudzbine) o;
        if(this.rbStavke != other.rbStavke || this.ukupnaVrednost != other.ukupnaVrednost || !this.artikal.equals(other.artikal))
            return false;
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "StavkaNarudzbine";
    }

    @Override
    public String vratiNaziveKolona() {
        return "narudzbinaID, kolicina, ukupnaVrednost, artikalId";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return narudzbinaId+", "+kolicina+", "+ukupnaVrednost+", "+artikal.getArtikalId();
    }

    @Override
    public String vratiVredostiZaInsert() {
        return rbStavke+", "+narudzbinaId+", "+kolicina+", "+ukupnaVrednost+", "+artikal.getArtikalId();
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "NarudzbinaID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(narudzbinaId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        return "(StavkaNarudzbine INNER JOIN Artikal ON StavkaNarudzbine.ArtikalID=Artikal.ArtikalID)";
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws SQLException {
        ArrayList<OpstiDomenskiObjekat> stavke = new ArrayList<>();
        while (rs.next()) {
            long rbStavke = rs.getLong("RbStavke");
            int kolicina = rs.getInt("Kolicina");
            double ukupnaVrednost = rs.getDouble("UkupnaVrednost");
            long artikalId = rs.getLong("ArtikalID");
            String naziv = rs.getString("Naziv");
            double cena = rs.getDouble("Cena");
            long narudzbinaID = rs.getLong("NarudzbinaID");
            Artikal artikal = new Artikal(artikalId, naziv, cena);
            StavkaNarudzbine stavka = new StavkaNarudzbine(rbStavke, kolicina, ukupnaVrednost, artikal, narudzbinaID);
            stavke.add(stavka);
        }
        return stavke;
    }

    @Override
    public String postaviVrednostAtributa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
