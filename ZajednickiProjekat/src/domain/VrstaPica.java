/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nemanja
 */
public class VrstaPica implements Serializable, OpstiDomenskiObjekat{
    
    private long vrstaPicaID;
    private String naziv;

    public VrstaPica() {
        setVrstaPicaID(Long.MIN_VALUE);
        setNaziv("Nepoznat");
    }

    public VrstaPica(long vrstaPicaID, String naziv) {
        setVrstaPicaID(vrstaPicaID);
        setNaziv(naziv);
    }

    public long getVrstaPicaID() {
        return vrstaPicaID;
    }

    public void setVrstaPicaID(long vrstaPicaID) {
        this.vrstaPicaID = vrstaPicaID;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public String toString() {
        return naziv;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        final VrstaPica other = (VrstaPica) o;
        if(this.vrstaPicaID != other.vrstaPicaID || !this.naziv.equals(other.naziv))
            return false;
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "VrstaPica";
    }

    @Override
    public String vratiNaziveKolona() {
        return "naziv";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return "'"+naziv+"'";
    }

    @Override
    public String vratiVredostiZaInsert() {
        return "'"+naziv+"'";
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "VrstaPicaID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(vrstaPicaID);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String postaviVrednostAtributa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
