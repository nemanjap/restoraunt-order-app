/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Nemanja
 */
public class Narudzbina implements Serializable, OpstiDomenskiObjekat {

    private long narudzbinaId;
    private Date datum;
    private int popust;
    private double ukupanIznos;
    private Sto sto;
    private Konobar konobar;
    private ArrayList<StavkaNarudzbine> stavkeNarudzbine;
    private boolean potvrdjena;
    private boolean spremna;
    private boolean izdatRacun;

    public Narudzbina() {
        narudzbinaId = Long.MIN_VALUE;
        stavkeNarudzbine = new ArrayList<>();
    }

    public Narudzbina(long narudzbinaId, Date datum, int popust, double ukupanIznos, Sto sto, Konobar konobar, ArrayList<StavkaNarudzbine> stavkeNarudzbine, boolean potvrdjena, boolean spremna, boolean izdatRacun) {
        setNarudzbinaId(narudzbinaId);
        setDatum(datum);
        setPopust(popust);
        setUkupanIznos(ukupanIznos);
        setSto(sto);
        setKonobar(konobar);
        setStavkeNarudzbine(stavkeNarudzbine);
        setSpremna(spremna);
        setPotvrdjena(potvrdjena);
        setIzdatRacun(izdatRacun);
    }

    public long getNarudzbinaId() {
        return narudzbinaId;
    }

    public void setNarudzbinaId(long narudzbinaId) {
        this.narudzbinaId = narudzbinaId;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getPopust() {
        return popust;
    }

    public void setPopust(int popust) {
        this.popust = popust;
    }

    public double getUkupanIznos() {
        return ukupanIznos;
    }

    public void setUkupanIznos(double ukupanIznos) {
        this.ukupanIznos = ukupanIznos;
    }

    public Sto getSto() {
        return sto;
    }

    public void setSto(Sto sto) {
        this.sto = sto;
    }

    public Konobar getKonobar() {
        return konobar;
    }

    public void setKonobar(Konobar konobar) {
        this.konobar = konobar;
    }

    public ArrayList<StavkaNarudzbine> getStavkeNarudzbine() {
        return stavkeNarudzbine;
    }

    public void setStavkeNarudzbine(ArrayList<StavkaNarudzbine> stavkeNarudzbine) {
        this.stavkeNarudzbine = stavkeNarudzbine;
    }

    public boolean isPotvrdjena() {
        return potvrdjena;
    }

    public void setPotvrdjena(boolean potvrdjena) {
        this.potvrdjena = potvrdjena;
    }

    public boolean isSpremna() {
        return spremna;
    }

    public void setSpremna(boolean spremna) {
        this.spremna = spremna;
    }

    public boolean isIzdatRacun() {
        return izdatRacun;
    }

    public void setIzdatRacun(boolean izdatRacun) {
        this.izdatRacun = izdatRacun;
    }

    @Override
    public String toString() {
        return narudzbinaId + ", '" + new java.sql.Date(datum.getTime()) + "', " + sto + ", " + konobar + ", " + popust + ", " + ukupanIznos;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final Narudzbina other = (Narudzbina) o;
        if (this.narudzbinaId != other.narudzbinaId) {
            return false;
        }
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "Narudzbina";
    }

    @Override
    public String vratiNaziveKolona() {
        return "datum, popust, ukupanIznos, StoId, konobarId, spremna, potvrdjena, izdatRacun";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return narudzbinaId+", '" + new java.sql.Date(datum.getTime()) + ", " + popust + ", " + ukupanIznos + ", " + sto.getStoId() + ", " + konobar.getKonobarId() + spremna + ", " + potvrdjena + ", " + izdatRacun;
    }

    @Override
    public String vratiVredostiZaInsert() {
        return narudzbinaId+", '" + new java.sql.Date(datum.getTime()) + "', " + popust + ", " + ukupanIznos + ", " + sto.getStoId() + ", " + konobar.getKonobarId() +", "+ spremna + ", " + potvrdjena + ", " + izdatRacun;
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "NarudzbinaID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(narudzbinaId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        return "(Narudzbina INNER JOIN Sto ON Narudzbina.StoID=Sto.StoID) INNER JOIN Konobar ON Narudzbina.KonobarID=Konobar.KonobarID";
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws SQLException {
        ArrayList<OpstiDomenskiObjekat> narudzbine = new ArrayList<>();
        while(rs.next()){
            long narudzbinaID = rs.getLong("NarudzbinaId");
            Date datum = rs.getTimestamp("Datum");
            int popust = rs.getInt("Popust");
            double ukupanIznos = rs.getDouble("UkupanIznos");
            boolean potvrdjena = rs.getBoolean("Potvrdjena");
            boolean spremna = rs.getBoolean("Spremna");
            boolean izdatRacun = rs.getBoolean("IzdatRacun");
            long stoID = rs.getLong("StoID");
            int brojMesta = rs.getInt("BrojMesta");
            Sto sto = new Sto(stoID, brojMesta);
            long konobarId = rs.getLong("KonobarId");
            String ime = rs.getString("Ime");
            String prezime = rs.getString("Prezime");
            Konobar konobar = new Konobar(konobarId, ime, prezime);
            Narudzbina narudzbina = new Narudzbina(narudzbinaID, datum, popust, ukupanIznos, sto, konobar, null, potvrdjena, spremna, izdatRacun);
            narudzbine.add(narudzbina);
        }
        
        return narudzbine;
        
    }

    @Override
    public String postaviVrednostAtributa() {
        return "NarudzbinaID=" + narudzbinaId + ", datum='" + new java.sql.Date(datum.getTime()) + "', popust=" + popust + ", ukupanIznos="
                + ukupanIznos + ", stoId=" + sto.getStoId() + ", konobarId=" + konobar.getKonobarId() + ", spremna=" + spremna + ", potvrdjena=" + potvrdjena + ", izdatRacun=" + izdatRacun;
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        return "DATEADD(\'d\', 0, DATEDIFF(\'d\', 0,datum)) = DATEADD(\'d\', 0, DATEDIFF(\'d\', 0, now())) and izdatRacun=false";
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        return stavkeNarudzbine.size();
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        return stavkeNarudzbine.get(i);
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        return "NarudzbinaID="+narudzbinaId;
    }

    @Override
    public String vratiIzdajRacun() {
        return "izdatRacun=" + true;
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        return "(StavkaNarudzbine INNER JOIN Artikal ON StavkaNarudzbine.ArtikalID=Artikal.ArtikalID)";
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) throws Exception {
        ArrayList<StavkaNarudzbine> stavke = new ArrayList<>();
        for (OpstiDomenskiObjekat odo1 : new StavkaNarudzbine().vratiListuIzResultSet(rs)) {
            System.out.println("Ima stavki##################################################################################");
            stavke.add((StavkaNarudzbine)odo1);
        }
        ((Narudzbina)odo).setStavkeNarudzbine(stavke);
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        return "NarudzbinaID="+narudzbinaId;
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        return "NarudzbinaID, Datum, Popust, UkupanIznos, Potvrdjena, Spremna, Narudzbina.StoID as StoID, BrojMesta, Konobar.KonobarID, Ime, Prezime";
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        return null;
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        return "StavkaNarudzbine";
    }
}
