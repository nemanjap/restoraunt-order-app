/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nemanja
 */
public class Pice extends Artikal implements Serializable,  OpstiDomenskiObjekat{
    
    private VrstaPica vrstaPica;

    public Pice() {
        setVrstaPica(new VrstaPica());
    }

    public Pice(VrstaPica vrstaPica) {
        setVrstaPica(vrstaPica);
    }

    public Pice(long artikalId, String naziv, double cena) {
        super(artikalId, naziv, cena);
        setVrstaPica(new VrstaPica());
    }

    public Pice(VrstaPica vrstaPica, long artikalId, String naziv, double cena) {
        super(artikalId, naziv, cena);
        setVrstaPica(vrstaPica);
    }

    public VrstaPica getVrstaPica() {
        return vrstaPica;
    }

    public void setVrstaPica(VrstaPica vrstaPica) {
        this.vrstaPica = vrstaPica;
    }

    @Override
    public String toString() {
        return getNaziv();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiImeTabele() {
        return "Jelo";
    }

    @Override
    public String vratiNaziveKolona() {
        return "artikalId, vrstaJelaId";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return Long.toString(getArtikalId()) + Long.toString(vrstaPica.getVrstaPicaID());
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        return "(Artikal INNER JOIN Pice ON(Artikal.ArtikalID=Pice.ArtikalID)) INNER JOIN VrstaPica ON(Pice.VrstaPicaID=VrstaPica.VrstaPicaID)";
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws Exception {
        ArrayList<OpstiDomenskiObjekat> pica = new ArrayList<>();
         while (rs.next()) {
            long artikalID = rs.getLong("ArtikalID");
            String nazivArtikla = rs.getString("nazivArtikla");
            double cenaArtikla = rs.getDouble("Cena");
            long vrstaPicaID = rs.getLong("VrstaPicaID");
            String nazivVrstePica = rs.getString("nazivPica");
            Pice pice = new Pice(new VrstaPica(vrstaPicaID, nazivVrstePica), artikalID, nazivArtikla, cenaArtikla);
            pica.add(pice);
        }
         return pica;
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        return "Artikal.ArtikalID, Artikal.Naziv AS nazivArtikla, Artikal.Cena, VrstaPica.VrstaPicaID, VrstaPica.Naziv AS nazivPica";
    }

    @Override
    public String vratiKriterijumPretrage() {
        return null;
    }
    
}
