/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Nemanja
 */
public class Racun implements Serializable, OpstiDomenskiObjekat{
    
    private long racunId;
    private Date datum;
    private double suma;
    private int obracunatiPopust;
    private double krajnjiIznos;
    private long narudzbinaId;
    private Narudzbina nar;

    public Racun(Narudzbina narudzbina) {
        this.datum = narudzbina.getDatum();
        this.suma = narudzbina.getUkupanIznos();
        this.obracunatiPopust = narudzbina.getPopust();
        this.krajnjiIznos = this.suma*(1-this.obracunatiPopust/100);
        this.narudzbinaId = narudzbina.getNarudzbinaId();
    }

    public Racun(long racunId, Date datum, double suma, int obracunatiPopust, double krajnjiIznos, long narudzbinaId) {
        this.racunId = racunId;
        this.datum = datum;
        this.suma = suma;
        this.obracunatiPopust = obracunatiPopust;
        this.krajnjiIznos = krajnjiIznos;
        this.narudzbinaId = narudzbinaId;
    }

    public long getRacunId() {
        return racunId;
    }

    public void setRacunId(long racunId) {
        this.racunId = racunId;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public double getSuma() {
        return suma;
    }

    public void setSuma(double suma) {
        this.suma = suma;
    }

    public int getObracunatiPopust() {
        return obracunatiPopust;
    }

    public void setObracunatiPopust(int obracunatiPopust) {
        this.obracunatiPopust = obracunatiPopust;
    }

    public double getKrajnjiIznos() {
        return krajnjiIznos;
    }

    public void setKrajnjiIznos(double krajnjiIznos) {
        this.krajnjiIznos = krajnjiIznos;
    }

    public long getNarudzbinaId() {
        return narudzbinaId;
    }

    public void setNarudzbinaId(long narudzbinaId) {
        this.narudzbinaId = narudzbinaId;
    }

    @Override
    public String toString() {
        return datum+", "+suma+", "+obracunatiPopust+", "+krajnjiIznos;
    }

    @Override
    public boolean equals(Object o) {
         if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        final Racun other = (Racun) o;
        if(this.racunId != other.racunId || this.narudzbinaId != other.narudzbinaId)
            return false;
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "Racun";
    }

    @Override
    public String vratiNaziveKolona() {
        return "datum, suma, obracunatiPopust, krajnjiIznos, narudzbinaId";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return datum+", "+suma+", "+obracunatiPopust+", "+krajnjiIznos+", "+narudzbinaId;
    }

    @Override
    public String vratiVredostiZaInsert() {
        return new java.sql.Date(datum.getTime())+", "+suma+", "+obracunatiPopust+", "+krajnjiIznos+", "+narudzbinaId;
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "RacunID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(racunId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String postaviVrednostAtributa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        long narID = narudzbinaId;
        Date dat = datum;
        int popust = obracunatiPopust;
        double krajnjiIzns = krajnjiIznos;
        
        Narudzbina nar = new Narudzbina();
        nar.setNarudzbinaId(narID);
        nar.setDatum(dat);
        nar.setUkupanIznos(krajnjiIzns);
        nar.setPopust(popust);
        
        return nar;
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
