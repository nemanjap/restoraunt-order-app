/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.sql.ResultSet;
import java.util.ArrayList;


/**
 *
 * @author Nemanja
 */
public interface OpstiDomenskiObjekat {
    
    public String vratiImeTabele();
    
    public String vratiNaziveTabelaJoin();
    
    public String vratiNaziveKolonaJoin();
    
    public String vratiNaziveKolona();
    
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws Exception;
    
    public String postaviVrednostAtributa();
    
    public String vratiUslovZaNadjiSlog();
    
    public int vratiBrojVezanihObjekata();
    
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i);
    
    public boolean vrednosnaOgranicenja();
    
    public String vratiKriterijumPretrage();
    
    public String vratiVrednostiAtributa();
    
    public String vratiVredostiZaInsert();
    
    public String vratiNazivIdentifikatora();
    
    public String vratiVrednostIdentifikatora();
    
    public String vratiIzdajRacun();
    
    public String vratiImeTabeleSlabogObjekta();
    
    public String vratiJoinTabeleSlabogObjekta();
    
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) throws Exception;
    
    public String vratiUslovZaPronalazakSlabogObjekta();
    
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat();
    
    
    
}
