/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nemanja
 */
public class Jelo extends Artikal implements Serializable, OpstiDomenskiObjekat {

    VrstaJela vrstaJela;

    public Jelo() {
        setVrstaJela(new VrstaJela());
    }

    public Jelo(VrstaJela vrstaJela) {
        setVrstaJela(vrstaJela);
    }

    public Jelo(long artikalId, String naziv, double cena) {
        super(artikalId, naziv, cena);
        setVrstaJela(new VrstaJela());
    }

    public Jelo(VrstaJela vrstaJela, long artikalId, String naziv, double cena) {
        super(artikalId, naziv, cena);
        setVrstaJela(vrstaJela);
    }

    public VrstaJela getVrstaJela() {
        return vrstaJela;
    }

    public void setVrstaJela(VrstaJela vrstaJela) {
        this.vrstaJela = vrstaJela;
    }

    @Override
    public String toString() {
        return getNaziv();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String vratiImeTabele() {
        return "Jelo";
    }

    @Override
    public String vratiNaziveKolona() {
        return "artikalId, vrstaJelaId";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return Long.toString(getArtikalId()) + Long.toString(vrstaJela.getVrstaJelaID());
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        return "(Artikal INNER JOIN Jelo ON(Artikal.ArtikalID=Jelo.ArtikalID)) INNER JOIN VrstaJela ON(Jelo.VrstaJelaID=VrstaJela.VrstaJelaID)";
    }

    
    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws Exception{
        ArrayList<OpstiDomenskiObjekat> jela = new ArrayList<>();
       while (rs.next()) {
            long artikalID = rs.getLong("ArtikalID");
            String nazivArtikla = rs.getString("nazivArtikla");
            double cenaArtikla = rs.getDouble("Cena");
            long vrstaJelaID = rs.getLong("VrstaJelaID");
            String nazivVrsteJela = rs.getString("nazivJela");
            Jelo jelo = new Jelo(new VrstaJela(vrstaJelaID, nazivVrsteJela), artikalID, nazivArtikla, cenaArtikla);
            jela.add(jelo);
        }
        return jela;
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        return "Artikal.ArtikalID, Artikal.Naziv AS nazivArtikla, Artikal.Cena, VrstaJela.VrstaJelaID, VrstaJela.Naziv AS nazivJela";
    }

    @Override
    public String vratiKriterijumPretrage() {
        return null;
    }
    
    
}
