/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nemanja
 */
public class Konobar implements Serializable, OpstiDomenskiObjekat{
    
    private long konobarId;
    private String ime;
    private String prezime;

    public Konobar() {
        setKonobarId(Long.MIN_VALUE);
        setIme("Nepoznato");
        setPrezime("Nepoznato");
    }

    public Konobar(long konobarId, String ime, String prezime) {
        setKonobarId(konobarId);
        setIme(ime);
        setPrezime(prezime);
    }
    
    public Konobar(String ime, String prezime) {
        setIme(ime);
        setPrezime(prezime);
    }

    public long getKonobarId() {
        return konobarId;
    }

    public void setKonobarId(long konobarId) {
        this.konobarId = konobarId;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @Override
    public String toString() {
        return ime+", "+prezime;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        final Konobar other = (Konobar) o;
        if(this.konobarId != other.konobarId || !this.ime.equals(other.ime) || !this.prezime.equals(other.prezime))
            return false;
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "Konobar";
    }

    @Override
    public String vratiNaziveKolona() {
        return "ime, prezime";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return "'"+ime+"', '"+prezime+"'";
    }

    @Override
    public String vratiVredostiZaInsert() {
        return "'"+ime+"', '"+prezime+"'";
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "KonobarID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(konobarId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String postaviVrednostAtributa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        return "konobarid, ime, prezime";
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
