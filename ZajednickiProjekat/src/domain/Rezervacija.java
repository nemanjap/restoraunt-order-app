/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Nemanja
 */
public class Rezervacija implements Serializable, OpstiDomenskiObjekat{
    
    private long rezervacijaId;
    private Sto sto;
    private String nosilacRezervacije;
    private Date datum;

    public Rezervacija() {
        setRezervacijaId(Long.MIN_VALUE);
        setSto(new Sto());
        setNosilacRezervacije("Nepoznat");
        setDatum(new Date());
    }

    public Rezervacija(long rezervacijaId, Sto sto, String nosilacRezervacije, Date datum) {
        setRezervacijaId(rezervacijaId);
        setSto(sto);
        setNosilacRezervacije(nosilacRezervacije);
        setDatum(datum);
    }
    
    public Rezervacija(Sto sto, String nosilacRezervacije, Date datum) {
        setSto(sto);
        setNosilacRezervacije(nosilacRezervacije);
        setDatum(datum);
    }

    public long getRezervacijaId() {
        return rezervacijaId;
    }

    public void setRezervacijaId(long rezervacijaId) {
        this.rezervacijaId = rezervacijaId;
    }

    public Sto getSto() {
        return sto;
    }

    public void setSto(Sto sto) {
        this.sto = sto;
    }

    public String getNosilacRezervacije() {
        return nosilacRezervacije;
    }

    public void setNosilacRezervacije(String nosilacRezervacije) {
        this.nosilacRezervacije = nosilacRezervacije;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    @Override
    public String toString() {
        return rezervacijaId+", "+datum+", "+nosilacRezervacije;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        final Rezervacija other = (Rezervacija) o;
        if(this.rezervacijaId != other.rezervacijaId || this.datum != other.datum || !this.nosilacRezervacije.equals(other.nosilacRezervacije))
            return false;
        return true;
    }

    @Override
    public String vratiImeTabele() {
        return "Rezervacija";
    }

    @Override
    public String vratiNaziveKolona() {
        return "stoId, datum, nosilacRezervacije";
    }

    @Override
    public String vratiVrednostiAtributa() {
        return sto.getStoId()+", '"+new java.sql.Date(datum.getTime())+"', "+"'"+nosilacRezervacije+"'";
    }

    @Override
    public String vratiVredostiZaInsert() {
        return sto.getStoId()+", '"+new java.sql.Date(datum.getTime())+"', "+"'"+nosilacRezervacije+"'";
    }

    @Override
    public String vratiNazivIdentifikatora() {
        return "RezervacijaID";
    }

    @Override
    public String vratiVrednostIdentifikatora() {
        return Long.toString(rezervacijaId);
    }

    @Override
    public String vratiNaziveTabelaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<OpstiDomenskiObjekat> vratiListuIzResultSet(ResultSet rs) throws SQLException {
        ArrayList<OpstiDomenskiObjekat> rezervacije = new ArrayList<>();
         while (rs.next()) {
            long stoId = rs.getLong("StoID");
            int brojMesta = rs.getInt("BrojMesta");
            long rezervacijaId = rs.getLong("RezervacijaID");
            String nosilacRezervacije = rs.getString("NosilacRezervacije");
            Date datum = rs.getTimestamp("Datum");
            rezervacije.add(new Rezervacija(rezervacijaId, new Sto(stoId, brojMesta), nosilacRezervacije, datum));
        }
         return rezervacije;
    }

    @Override
    public String postaviVrednostAtributa() {
        return "StoID="+sto.getStoId()+", nosilacRezervacije='"+nosilacRezervacije+"', datum='"+new java.sql.Date(datum.getTime())+"'";
    }

    @Override
    public String vratiUslovZaNadjiSlog() {
        return "RezervacijaID="+rezervacijaId;
    }

    @Override
    public int vratiBrojVezanihObjekata() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiSlogVezanogObjekta(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean vrednosnaOgranicenja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiKriterijumPretrage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiIzdajRacun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiJoinTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setujPronadjeneSlabeObjekte(ResultSet rs, OpstiDomenskiObjekat odo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiUslovZaPronalazakSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiNaziveKolonaJoin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OpstiDomenskiObjekat vratiPovezaniSlozeniObjekat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String vratiImeTabeleSlabogObjekta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
