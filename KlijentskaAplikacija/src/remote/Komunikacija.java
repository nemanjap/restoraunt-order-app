/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package remote;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Nemanja
 */
public class Komunikacija {
    
    private static Komunikacija instance;
    private Socket soket;
    ObjectOutputStream out;
    ObjectInputStream in;
    private Komunikacija() throws UnknownHostException, IOException{
        soket = new Socket("localhost", 9000);
        out = new ObjectOutputStream(soket.getOutputStream());
        in = new ObjectInputStream(soket.getInputStream());
    }
    
    public static Komunikacija getInstance() throws UnknownHostException, IOException{
        if(instance == null)
            instance = new Komunikacija();
        return instance;
    }
    
    public TransferniObjekat posaljiPoruku(int tipPoruke, Object parametar) throws Exception{
        System.out.println("Komunikacija: Slanje poruke zapoceto...");
        System.out.println("Komunikacija: Kreiram input i output stream...");
        //ObjectOutputStream out = new ObjectOutputStream(soket.getOutputStream());
        //ObjectInputStream in = new ObjectInputStream(soket.getInputStream());
        System.out.println("Komunikacija: Streamovi kreirani...");
        TransferniObjekat to = new TransferniObjekat(tipPoruke, parametar);
        System.out.println("Komunikacija: Transferni objekat je kreiran...");
        System.out.println("Komunikacija: Saljem serveru objekat...");
        out.writeObject(to);
        System.out.println("Komunikacija: Transferni objekat je poslat...");
        System.out.println("Komunikacija: Cekam na odgovor servera...");
        to = (TransferniObjekat) in.readObject();
        System.out.println("Komunikacija: Primljen odgovor servera...");
        if(to.getIzuzetak() != null){
            Exception e = (Exception) to.getIzuzetak();
            throw e;
        }
        return to;
    }
    
        public void resetujKonekciju(){
        soket = null;
        instance = null;
        System.out.println("Komunikacija: Konekcija je prekinuta. Logovanje nije bilo uspesno.");
    }
    
        
        
}
