/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.forms.glavna_forma.kontrolerki;

import javax.swing.JPanel;
import view.forms.glavna_forma.FormGlavna;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIFormaGlavna {
    
    private FormGlavna forma;
    
    private KontrolerKIFormaGlavna(){}
    
    private static KontrolerKIFormaGlavna instance;
    
    public static KontrolerKIFormaGlavna getInstance(){
        if(instance == null)
            instance = new KontrolerKIFormaGlavna();
        return instance;
    }

    public FormGlavna getForma() {
        return forma;
    }

    public void setForma(FormGlavna forma) {
        this.forma = forma;
    }
    
    public void postaviPanel(JPanel panel) {
        forma.getContentPane().removeAll();
        forma.getContentPane().add(panel);
        forma.getContentPane().doLayout();
        forma.update(forma.getGraphics());
    }
    
}
