/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.models;

import domain.Narudzbina;
import domain.StavkaNarudzbine;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nemanja
 */
public class TabelModelStavkeNarudzbine extends AbstractTableModel {

    private Narudzbina narudzbina;
    private String[] columnNames = new String[]{"Naziv Artikla", "Kolicina"};

    public TabelModelStavkeNarudzbine(Narudzbina narudzbina) {
        this.narudzbina = narudzbina;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames[i];
    }

    @Override
    public int getRowCount() {
        return narudzbina.getStavkeNarudzbine().size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        StavkaNarudzbine sn = narudzbina.getStavkeNarudzbine().get(i);
        switch (i1) {
            case 0:
                return sn.getArtikal().getNaziv();
            case 1:
                return sn.getKolicina();
            default:
                return "Greska";
        }
    }

    public void povecaj(int index) {
        int kolicina = narudzbina.getStavkeNarudzbine().get(index).getKolicina();
        kolicina += 1;
        narudzbina.getStavkeNarudzbine().get(index).setKolicina(kolicina);
        fireTableDataChanged();
    }

    public void smanji(int index) {
        int kolicina = narudzbina.getStavkeNarudzbine().get(index).getKolicina();
        kolicina -= 1;
        if (kolicina <= 0) {
            return;
        }
        narudzbina.getStavkeNarudzbine().get(index).setKolicina(kolicina);
        fireTableDataChanged();
    }

    public void obrisi(int index) {
        if (index < narudzbina.getStavkeNarudzbine().size()) {
            narudzbina.getStavkeNarudzbine().remove(index);
            fireTableDataChanged();
        }

    }
}
