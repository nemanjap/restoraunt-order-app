/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.models;

import domain.Rezervacija;
import domain.Sto;
import java.text.SimpleDateFormat;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nemanja
 */
public class TableModelRezervacijeStola extends AbstractTableModel {

    private Sto sto;
    private String[] columnNames = new String[]{"StoID", "RezervacijaID", "Nosilac Rezervacije", "Datum", "Vreme"};
    private SimpleDateFormat dfVreme = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat dfDatum = new SimpleDateFormat("dd. MM. yyyy.");
    
    public TableModelRezervacijeStola(Sto sto) {
        this.sto = sto;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames[i];
    }

    @Override
    public int getRowCount() {
        return sto.getRezervacije().size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Rezervacija rezervacija = sto.getRezervacije().get(i);
        switch (i1) {
            case 0:
                return rezervacija.getSto().getStoId();
            case 1:
                return rezervacija.getRezervacijaId();
            case 2:
                return rezervacija.getNosilacRezervacije();
            case 3:
                return dfDatum.format(rezervacija.getDatum());
            case 4:
                return dfVreme.format(rezervacija.getDatum());
            default:
                return "Greska";
        }
    }
}
