/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.models;

import domain.Artikal;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nemanja
 */
public class TableModelArtikli extends AbstractTableModel{
    
    private ArrayList<Artikal> artikli = new ArrayList<>();
    private String[] columnNames = new String[]{"Naziv Artikla", "Cena"};

    public TableModelArtikli(ArrayList<Artikal> artikli) {
        this.artikli = artikli;
    }

    
    
    @Override
    public String getColumnName(int i) {
        return columnNames[i];
    }

    @Override
    public int getRowCount() {
        return artikli.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Artikal artikal = artikli.get(i);
        switch(i1){
            case 0: return artikal.getNaziv();
            case 1: return artikal.getCena();
            default: return "Greska";
        }
    }
    
    public Artikal vratiSelektovaniArtikal(int index){
        return artikli.get(index);
    }
    
}
