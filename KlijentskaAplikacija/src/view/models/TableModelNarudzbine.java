/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.models;

import domain.Narudzbina;
import domain.Sto;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nemanja
 */
public class TableModelNarudzbine extends AbstractTableModel{
    
    private ArrayList<Sto> stolovi;
    
    public ArrayList<Sto> getStolovi() {
        return stolovi;
    }

    public void setStolovi(ArrayList<Sto> stolovi) {
        this.stolovi = stolovi;
    }
    private String[] columnNames = new String[]{"ID", "Sto", "Konobar"};
    
    public TableModelNarudzbine(ArrayList<Sto> stolovi){
        this.stolovi = stolovi;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames[i];
    }

    @Override
    public int getRowCount() {
        return stolovi.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Narudzbina nar = stolovi.get(i).getNarudzbina();
        switch(i1){
            case 0: return nar.getNarudzbinaId();
            case 1: return nar.getSto();
            case 2: return nar.getKonobar();
            default: return "Greska";
        }
    }
    
    public Sto vratiObjekat(int index){
        return stolovi.get(index);
    }
    
}
