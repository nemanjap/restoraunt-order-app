/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.models;

import domain.Sto;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nemanja
 */
public class TableModelSlobodniStolovi extends AbstractTableModel {

    private ArrayList<Sto> stolovi;
    private String[] columnNames = new String[]{"Broj Mesta", "StoID"};

    public TableModelSlobodniStolovi(ArrayList<Sto> stolovi) {
        this.stolovi = stolovi;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames[i];
    }

    @Override
    public int getRowCount() {
        return stolovi.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Sto sto = stolovi.get(i);
        switch (i1) {
            case 0:
                return sto.getBrojMesta();
            case 1:
                return sto.getStoId();
            default:
                return "Greska";
        }
    }
    
    public Sto vratiObjekat(int index){
        return stolovi.get(index);
    }
}
