/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.login.kontrolerki;

import crypto.AESCrypt;
import domain.Konobar;
import domain.Korisnik;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import niti.KonobarRefresh;
import remote.Komunikacija;
import view.filters.DocumentFilterPatternFilter;
import view.forms.glavna_forma.kontrolerki.KontrolerKIFormaGlavna;
import view.panels.konobar.PanelGlavniKonobar;
import view.panels.konobar.kontrolerki.KontrolerKIPanelGlavniKonobar;
import view.panels.login.PanelLogin;
import view.panels.rezervacije.PanelGlavniRezervacije;

/**
 *
 * @author Nemanja
 */
public class KontrolerKILogin {

    private static KontrolerKILogin instance;
    private PanelLogin panel;
    private Socket soket;

    public Socket getSoket() {
        return soket;
    }

    public void setSoket(Socket soket) {
        this.soket = soket;
    }

    public PanelLogin getPanel() {
        return panel;
    }

    public void setPanel(PanelLogin panel) {
        this.panel = panel;
    }

    private KontrolerKILogin() {
    }

    public static KontrolerKILogin getInstance() {
        if (instance == null) {
            instance = new KontrolerKILogin();
        }
        return instance;
    }

    public void login(JTextField usernameField, JTextField passwordField) {
        try {
            System.out.println("KontrolerLogin: Login zapocet...");
            if (usernameField.getText().isEmpty() || passwordField.getText().isEmpty()) {
                System.out.println("KontrolerLogin: Login prekinut usled praznih polja...");
                if (usernameField.getText().isEmpty()) {
                    panel.getLabelUsernameField().setText(" Morate uneti korisnicko ime.");
                    panel.getLabelUsernameField().setVisible(true);
                    usernameField.requestFocus();
                    return;
                }
                if (passwordField.getText().isEmpty()) {
                    panel.getLabelPasswordField().setText(" Morate uneti lozinku");
                    panel.getLabelPasswordField().setVisible(true);
                    passwordField.requestFocus();
                    return;
                }

                return;
            }

            System.out.println("KontrolerLogin: Provera polja uspesna...");
            String username = usernameField.getText();
            String password = AESCrypt.encrypt(passwordField.getText());
            usernameField.requestFocus();
            Korisnik zaSlanje = new Korisnik(username, password);
            System.out.println("KontrolerLogin: Spakovao username i password...");
            Korisnik odgovor = (Korisnik) Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.LOGIN, zaSlanje).getRezultat();
            System.out.println("KontrolerLogin: Dobio odgovor od servera: " + odgovor);
            if (odgovor == null) {
                Komunikacija.getInstance().resetujKonekciju();
                resetujPolja();
                panel.getLabelPasswordField().setText(" Korisnicko ime ili lozinka nisu ispravni");
                return;
            }
            System.out.println("KontrolerKILOgin: Obradjujem tip korisnika...");
            switch (odgovor.getUloga().getNazivUloge()) {
                case "prijem":
                    KontrolerKIFormaGlavna.getInstance().postaviPanel(new PanelGlavniRezervacije());
                    break;
                case "konobar":
                    long id = odgovor.getKorisnikId();
                    String ime = odgovor.getIme();
                    String prezime = odgovor.getPrezime();
                    Konobar k = new Konobar(id, ime, prezime);
                    KontrolerKIFormaGlavna.getInstance().postaviPanel(new PanelGlavniKonobar());
                    KontrolerKIPanelGlavniKonobar.getInstance().setKonobar(k);
                    new KonobarRefresh().start();
                    break;
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            panel.getLabelPasswordField().setText(" Server trenutno nije dostupan");
            panel.getLabelPasswordField().setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKILogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void prikazNeuspesanLogin() {
        panel.getLabelPasswordField().setText(" Korisnicko ime ili lozinka nisu ispravni");
        panel.getLabelPasswordField().setVisible(true);
    }

    public void inicijalizacijaIzgleda() {
        //postavljanje padding-a
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        //postavljanje Document Filtera kako bi se dozvolio unos samo lower case slova
        AbstractDocument document = (AbstractDocument) panel.getTextFieldUsername().getDocument();
        document.setDocumentFilter(new DocumentFilterPatternFilter("^[a-z]{0,15}$"));
        //postavljanje ogranicenja na polje za unos lozinke.
        //Dozvoljen unos samo slova(Uppercas i Lowecase) i brojeva
        AbstractDocument d1 = (AbstractDocument) panel.getTextFieldPassword().getDocument();
        d1.setDocumentFilter(new DocumentFilterPatternFilter("^[a-zA-Z0-9]{0,15}$"));
        //postavljanje Action LIstener-a za polje password da pritiskom na enter krene login proces
        //prvo za polje za unos imena 
        panel.getTextFieldUsername().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                panel.getLabelPasswordField().setVisible(false);
                login(panel.getTextFieldUsername(), panel.getTextFieldPassword());
            }
        });
        // pa za polje za unos lozinke
        panel.getTextFieldPassword().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                panel.getTextFieldUsername().requestFocus();
                login(panel.getTextFieldUsername(), panel.getTextFieldPassword());
            }
        });
        panel.getLabelUsernameField().setVisible(false);
        panel.getLabelPasswordField().setVisible(false);

        panel.getTextFieldUsername().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent de) {
                panel.getLabelUsernameField().setVisible(false);
                panel.getTextFieldUsername().setBackground(new Color(136, 136, 136, 255));
                panel.getLabelPasswordField().setVisible(false);
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                panel.getLabelUsernameField().setVisible(false);
                panel.getTextFieldUsername().setBackground(new Color(136, 136, 136, 255));
                panel.getLabelPasswordField().setVisible(false);
                if (panel.getTextFieldUsername().getText().isEmpty()) {
                    panel.getTextFieldUsername().setBackground(Color.PINK);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent de) {
            }
        });

        panel.getTextFieldPassword().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent de) {
                panel.getLabelPasswordField().setVisible(false);
                panel.getTextFieldPassword().setBackground(new Color(136, 136, 136, 255));
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                panel.getLabelPasswordField().setVisible(false);
                panel.getTextFieldPassword().setBackground(new Color(136, 136, 136, 255));
                if (panel.getTextFieldPassword().getPassword().length == 0) {
                    panel.getTextFieldPassword().setBackground(Color.PINK);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent de) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    public void resetujPolja() {
        panel.getTextFieldUsername().setText("");
        panel.getTextFieldPassword().setText("");
    }
}
