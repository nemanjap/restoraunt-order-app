/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.konobar.kontrolerki;

import domain.Jelo;
import domain.Konobar;
import domain.Narudzbina;
import domain.Pice;
import domain.Racun;
import domain.StavkaNarudzbine;
import domain.Sto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import remote.Komunikacija;
import utils.Capitalization;
import view.dialogs.OkDialog;
import view.models.TabelModelStavkeNarudzbine;
import view.models.TableModelNarudzbine;
import view.panels.konobar.PanelGlavniKonobar;
import view.panels.rezervacije.kontrolerki.KontrolerKIGlavniRezervacije;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIPanelGlavniKonobar {

    private Konobar konobar;
    private PanelGlavniKonobar panel;
    private ArrayList<Sto> stolovi;
    private static KontrolerKIPanelGlavniKonobar instance;
    private ArrayList<Jelo> jela;
    private ArrayList<Pice> pica;

    public PanelGlavniKonobar getPanel() {
        return panel;
    }

    public void setPanel(PanelGlavniKonobar panel) {
        this.panel = panel;
    }

    public ArrayList<Sto> getStolovi() {
        return stolovi;
    }

    public void setStolovi(ArrayList<Sto> stolovi) {
        this.stolovi = stolovi;
    }

    public ArrayList<Jelo> getJela() {
        return jela;
    }

    public void setJela(ArrayList<Jelo> jela) {
        this.jela = jela;
    }

    public ArrayList<Pice> getPica() {
        return pica;
    }

    public void setPica(ArrayList<Pice> pica) {
        this.pica = pica;
    }

    public Konobar getKonobar() {
        return konobar;
    }

    public void setKonobar(Konobar konobar) {
        this.konobar = konobar;
    }

    private KontrolerKIPanelGlavniKonobar() {
    }

    public static KontrolerKIPanelGlavniKonobar getInstance() {
        if (instance == null) {
            instance = new KontrolerKIPanelGlavniKonobar();
        }
        return instance;
    }

    public void ucitajStolove() {
        try {
            stolovi = (ArrayList<Sto>) Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.VRATI_SVE_STOLOVE, new Sto()).getRezultat();

            System.out.println("KontrolerGlavniKonobar: Stolovi su ucitani...");
            panel.getComboBoxStolovi().setModel(new DefaultComboBoxModel(stolovi.toArray()));
            System.out.println("KontrolerGlavniKonobar: model postavljen");
            postaviModelTabeleNarudzbina(filtrirajStolovePoNarudzbini(stolovi));
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIGlavniRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIGlavniRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIGlavniRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void postaviModelTabeleStavki() {
        Sto izabraniSto = (Sto) panel.getComboBoxStolovi().getSelectedItem();
        panel.getTablePregledStavkiNarudzbine().setModel(new TabelModelStavkeNarudzbine(izabraniSto.getNarudzbina()));
    }

    public void postaviModelTabeleNarudzbina(ArrayList<Sto> stolovi) {
        panel.getTablePregleNarudzbina().setModel(new TableModelNarudzbine(stolovi));
    }

    public void posaljiZahtevZaArtiklima() {
        try {
            Object[] o = (Object[]) Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.VRATI_SVE_ARTIKLE, null).getRezultat();
            pica = (ArrayList<Pice>) o[1];
            jela = (ArrayList<Jelo>) o[0];
            System.out.println("KontrolerUnesiArtikle: Artikli su uspesno ucitani...");
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIPanelUnesiArtikle.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIPanelUnesiArtikle.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIPanelUnesiArtikle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Sto> filtrirajStolovePoNarudzbini(ArrayList<Sto> stolovi) {
        ArrayList<Sto> filtriraniStolovi = new ArrayList<>();
        for (Sto sto : stolovi) {
            if (sto.getNarudzbina().getNarudzbinaId() != Long.MIN_VALUE) {
                filtriraniStolovi.add(sto);
            }
        }
        return filtriraniStolovi;
    }

    public void inicijalizujIzgled() {
        panel.getComboBoxStolovi().removeAllItems();

        panel.getTablePregledStavkiNarudzbine().setModel(new TabelModelStavkeNarudzbine(new Narudzbina()));

        panel.getComboBoxStolovi().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                postaviModelTabeleStavki();
                if (((Sto) panel.getComboBoxStolovi().getSelectedItem()).getNarudzbina().isPotvrdjena()) {
                    panel.getButtonOtkazi().setEnabled(false);
                } else {
                    panel.getButtonOtkazi().setEnabled(true);
                }
                if (((Sto) panel.getComboBoxStolovi().getSelectedItem()).getNarudzbina().isSpremna()) {
                    panel.getButtonIzdajRacun().setEnabled(true);
                } else {
                    panel.getButtonIzdajRacun().setEnabled(false);
                }
            }
        });

        panel.getTextFieldKriterijumPretrage().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent de) {
                pretraziNarudzbine();
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                pretraziNarudzbine();
            }

            @Override
            public void changedUpdate(DocumentEvent de) {
                pretraziNarudzbine();
            }
        });

        ucitajStolove();
        panel.getComboBoxStolovi().setSelectedIndex(0);
    }

    private void pretraziNarudzbine() {
        if (panel.getTextFieldKriterijumPretrage().getText().isEmpty()) {
            postaviModelTabeleNarudzbina(filtrirajStolovePoNarudzbini(stolovi));
            return;
        }
        ArrayList<Sto> pretrazeniStolovi = new ArrayList<>();
        String kriterijumPretrage = Capitalization.capitalizeFully(panel.getTextFieldKriterijumPretrage().getText(), null);
        ArrayList<Sto> stoloviZaPretragu = filtrirajStolovePoNarudzbini(stolovi);
        for (Sto sto : stoloviZaPretragu) {
            if (sto.getNarudzbina().getKonobar().getIme().startsWith(kriterijumPretrage) || sto.getNarudzbina().getKonobar().getPrezime().startsWith(kriterijumPretrage) || Integer.toString(sto.getBrojMesta()).equals(kriterijumPretrage) || Long.toString(sto.getStoId()).equals(kriterijumPretrage)) {
                pretrazeniStolovi.add(sto);
            }
        }
        postaviModelTabeleNarudzbina(pretrazeniStolovi);
        panel.getTextFieldKriterijumPretrage().requestFocus();
    }

    public void prikazi() {
        int index = panel.getTablePregleNarudzbina().getSelectedRow();

        if (index < 0 || index > panel.getTablePregleNarudzbina().getRowCount()) {
            return;
        }
        panel.getComboBoxStolovi().setSelectedItem(((TableModelNarudzbine) panel.getTablePregleNarudzbina().getModel()).vratiObjekat(index));
    }

    public void povecaj() {
        int index = panel.getTablePregledStavkiNarudzbine().getSelectedRow();
        if (index < 0 || index > panel.getTablePregledStavkiNarudzbine().getRowCount()) {
            return;
        }
        ((TabelModelStavkeNarudzbine) panel.getTablePregledStavkiNarudzbine().getModel()).povecaj(index);
        panel.getTablePregledStavkiNarudzbine().setRowSelectionInterval(index, index);
    }

    public void smanji() {
        int index = panel.getTablePregledStavkiNarudzbine().getSelectedRow();
        if (index < 0 || index > panel.getTablePregledStavkiNarudzbine().getRowCount()) {
            return;
        }
        ((TabelModelStavkeNarudzbine) panel.getTablePregledStavkiNarudzbine().getModel()).smanji(index);
        panel.getTablePregledStavkiNarudzbine().setRowSelectionInterval(index, index);
    }

    public void odbisi() {
        int index = panel.getTablePregledStavkiNarudzbine().getSelectedRow();
        if (index < 0 || index > panel.getTablePregledStavkiNarudzbine().getRowCount()) {
            return;
        }
        ((TabelModelStavkeNarudzbine) panel.getTablePregledStavkiNarudzbine().getModel()).obrisi(index);
        panel.getTablePregledStavkiNarudzbine().setRowSelectionInterval(0, 0);
    }

    public void otkazi() {
        Sto sto = (Sto) panel.getComboBoxStolovi().getSelectedItem();
        if (sto.getNarudzbina().isPotvrdjena()) {
            new OkDialog(null, "Greska", "Narudzbina je vec potvrdjena", true, OkDialog.ERROR);
            return;
        }
        sto.setNarudzbina(new Narudzbina());
        int indexStola = KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().indexOf(sto);
        KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().get(indexStola).getNarudzbina().setStavkeNarudzbine(sto.getNarudzbina().getStavkeNarudzbine());
        KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleNarudzbina(KontrolerKIPanelGlavniKonobar.getInstance().filtrirajStolovePoNarudzbini(KontrolerKIPanelGlavniKonobar.getInstance().getStolovi()));
        KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleStavki();
    }

    public void potvrdi() {
        try {
            Sto sto = (Sto) panel.getComboBoxStolovi().getSelectedItem();
            if (sto.getNarudzbina().getNarudzbinaId() == Long.MIN_VALUE || sto.getNarudzbina().getStavkeNarudzbine().isEmpty()) {
                System.out.println("KontrolerGlavniKonobar: Proces potvrde rezervacije prekinut usled nepostojanja narudzbine...");
                return;
            }
            
            for (StavkaNarudzbine st : sto.getNarudzbina().getStavkeNarudzbine()) {
                System.out.println(st);
            }
            System.out.println("########################################################################");
            sto.getNarudzbina().setPotvrdjena(true);
            System.out.println(sto);
            System.out.println(sto.getNarudzbina());
            for(StavkaNarudzbine sn : sto.getNarudzbina().getStavkeNarudzbine()){
                System.out.println(sn);
            }
            Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.POTVRDI_NARUDZBINU, sto.getNarudzbina());
            new OkDialog(null, "Uspeh", "Narudzbina za sto " + sto.getStoId() + " je uspesno potvrdjena", true, OkDialog.SUCCESS);
            panel.getButtonOtkazi().setEnabled(false);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public long vratiIDNarudzbine() {
        long id = 0;
        try {
            id = (Long) (Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.VRATI_ID_NARUDZBINE, new Narudzbina()).getRezultat());
            return id;
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public void osveziListuNarudzbina() {
        ucitajStolove();
        panel.getComboBoxStolovi().setSelectedIndex(0);
    }

    public void sacuvajRacun() {
        try {
            Sto sto = (Sto) panel.getComboBoxStolovi().getSelectedItem();

            if (sto.getNarudzbina().getNarudzbinaId() == Long.MIN_VALUE || sto.getNarudzbina().getStavkeNarudzbine().isEmpty()) {
                System.out.println("KontrolerGlavniKonobar: Proces izdavanja racuna rezervacije prekinut usled nepostojanja narudzbine...");
                return;
            }
            
            Narudzbina nar = sto.getNarudzbina();
            
            double suma = 0;
            double krajnjiIznos = 0;
            
            for(StavkaNarudzbine sn : nar.getStavkeNarudzbine()){
                suma+=sn.getKolicina()*sn.getArtikal().getCena();
                System.out.println(suma);
            }
            
            krajnjiIznos = (1-nar.getPopust()/100)*suma;
            System.out.println(krajnjiIznos);
            
            Racun racun = new Racun(0, new Date(), suma, nar.getPopust(), krajnjiIznos, nar.getNarudzbinaId());
            
            

            Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.SACUVAJ_RACUN, racun);
            new OkDialog(null, "Uspeh", "Racun za sto " + sto.getStoId() + " je uspesno sacuvan", true, OkDialog.SUCCESS);

            sto.setNarudzbina(new Narudzbina());
            int indexStola = KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().indexOf(sto);
            KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().get(indexStola).getNarudzbina().setStavkeNarudzbine(sto.getNarudzbina().getStavkeNarudzbine());
            KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleNarudzbina(KontrolerKIPanelGlavniKonobar.getInstance().filtrirajStolovePoNarudzbini(KontrolerKIPanelGlavniKonobar.getInstance().getStolovi()));
            KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleStavki();
            panel.getComboBoxStolovi().setSelectedIndex(0);

        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIPanelGlavniKonobar.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}
