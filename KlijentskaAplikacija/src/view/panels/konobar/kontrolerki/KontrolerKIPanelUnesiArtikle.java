/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.konobar.kontrolerki;

import domain.Artikal;
import domain.Jelo;
import domain.Narudzbina;
import domain.Pice;
import domain.StavkaNarudzbine;
import domain.Sto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.SpinnerListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import view.dialogs.OkDialog;
import view.models.TableModelArtikli;
import view.panels.konobar.PanelUnesiArtikle;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIPanelUnesiArtikle {

    private ArrayList<Jelo> jela;
    private ArrayList<Pice> pica;
    private static KontrolerKIPanelUnesiArtikle instance;
    private PanelUnesiArtikle panel;
    private Sto sto;
    private int rbStavke;

    public PanelUnesiArtikle getPanel() {
        return panel;
    }

    public void setPanel(PanelUnesiArtikle panel) {
        this.panel = panel;
    }

    public Sto getSto() {
        return sto;
    }

    public void setSto(Sto sto) {
        this.sto = sto;
        if (sto.getNarudzbina().getStavkeNarudzbine().isEmpty()) {
            System.out.println("Prazan sto");
            sto.setNarudzbina(new Narudzbina(KontrolerKIPanelGlavniKonobar.getInstance().vratiIDNarudzbine() + 1, new Date(), 0, 0, sto, KontrolerKIPanelGlavniKonobar.getInstance().getKonobar(), new ArrayList<StavkaNarudzbine>(),false, false,false));
            rbStavke = 0;
        } else {
            System.out.println("Zauzet sto");
            rbStavke = sto.getNarudzbina().getStavkeNarudzbine().size();
        }
        System.out.println(sto);
    }

    private KontrolerKIPanelUnesiArtikle() {
    }

    public static KontrolerKIPanelUnesiArtikle getInstance() {
        if (instance == null) {
            instance = new KontrolerKIPanelUnesiArtikle();
        }
        return instance;
    }

    public void postaviIzgled() {

        panel.getComboBoxGrupaArtikala().removeAllItems();
        panel.getTableArtikli().setModel(new TableModelArtikli(new ArrayList<Artikal>()));
        SpinnerListModel spinerModel = new SpinnerListModel(vratiDozvoljeniOpseg());
        panel.getSpinnerKolicina().setModel(spinerModel);

        jela = KontrolerKIPanelGlavniKonobar.getInstance().getJela();
        pica = KontrolerKIPanelGlavniKonobar.getInstance().getPica();

        String[] naziviGrupaJela = new String[]{"Predjela", "Supe", "Glavna Jela", "Salate", "Dezerti", "Alkoholna Pica", "Bezalkohola Pica"};
        panel.getComboBoxGrupaArtikala().setModel(new DefaultComboBoxModel(naziviGrupaJela));
        postaviModelTabeleArtikala();

        panel.getComboBoxGrupaArtikala().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                postaviModelTabeleArtikala();
            }
        });
        panel.getLabelGreska().setVisible(false);
        panel.getLabelGreskaKolicina().setVisible(false);
        panel.getSpinnerKolicina().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                panel.getLabelGreskaKolicina().setVisible(false);
            }
        });
    }

    private void postaviModelTabeleArtikala() {
        String nazivTipaArtikla = (String) panel.getComboBoxGrupaArtikala().getSelectedItem();
        ArrayList<Artikal> artikli = new ArrayList<>();
        switch (nazivTipaArtikla) {
            case "Predjela":
                for (Jelo jelo : jela) {
                    if (jelo.getVrstaJela().getNaziv().equals("Predjelo")) {
                        artikli.add(jelo);
                    }
                }
                break;
            case "Supe":
                for (Jelo jelo : jela) {
                    if (jelo.getVrstaJela().getNaziv().equals("Supa")) {
                        artikli.add(jelo);
                    }
                }
                break;
            case "Glavna Jela":
                for (Jelo jelo : jela) {
                    if (jelo.getVrstaJela().getNaziv().equals("GlavnoJelo")) {
                        artikli.add(jelo);
                    }
                }
                break;
            case "Salate":
                for (Jelo jelo : jela) {
                    if (jelo.getVrstaJela().getNaziv().equals("Salata")) {
                        artikli.add(jelo);
                    }
                }
                break;
            case "Dezerti":
                for (Jelo jelo : jela) {
                    if (jelo.getVrstaJela().getNaziv().equals("Dezert")) {
                        artikli.add(jelo);
                    }
                }
                break;
            case "Alkoholna Pica":
                for (Pice pice : pica) {
                    if (pice.getVrstaPica().getNaziv().equals("Alkoholno")) {
                        artikli.add(pice);
                    }
                }
                break;
            case "Bezalkohola Pica":
                for (Pice pice : pica) {
                    if (pice.getVrstaPica().getNaziv().equals("Bezalkoholno")) {
                        artikli.add(pice);
                    }
                }
                break;
            default:
                break;
        }
        panel.getTableArtikli().setModel(new TableModelArtikli(artikli));
    }

    private String[] vratiDozvoljeniOpseg() {
        String[] minuti = new String[20];
        for (int i = 0; i < minuti.length; i++) {
            minuti[i] = Integer.toString(i + 1);
        }
        return minuti;
    }

    public void dodajArtikal() {
        panel.getLabelGreska().setVisible(false);
        panel.getLabelGreskaKolicina().setVisible(false);
        if (panel.getTableArtikli().getSelectedRow() == -1) {
            panel.getLabelGreska().setText(" Morate izabrati artikal iz tabele");
            panel.getLabelGreska().setVisible(true);
            return;
        }
        if (panel.getSpinnerKolicina().getValue().equals("0")) {
            panel.getLabelGreskaKolicina().setText(" Morate uneti kolicinu");
            panel.getLabelGreskaKolicina().setVisible(true);
            return;
        }
        Artikal artiklal = vratiIzabraniArtikalIzTabeleArtikala();
        int kolicina = Integer.parseInt(panel.getSpinnerKolicina().getValue().toString());
        for (StavkaNarudzbine stavka : sto.getNarudzbina().getStavkeNarudzbine()) {
            if (stavka.getArtikal().getArtikalId() == (artiklal.getArtikalId())) {
                System.out.println("Artikal je vec dodat povecavam kolicinu....");
                stavka.setKolicina(stavka.getKolicina() + kolicina);
                KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleStavki();
                panel.getLabelGreska().setIcon(new ImageIcon("src/slike/ok1.png"));
                panel.getLabelGreska().setText("Kolicina artikla je uspesno povecana");
                panel.getLabelGreska().setVisible(true);
                panel.getSpinnerKolicina().setValue("1");
                return;
            }
        }
        rbStavke += 1;
        long redniBrojStavke = rbStavke;
        double ukupnaVrednost = kolicina * artiklal.getCena();
        StavkaNarudzbine stavka = new StavkaNarudzbine(redniBrojStavke, kolicina, ukupnaVrednost, artiklal, sto.getNarudzbina().getNarudzbinaId());
        sto.getNarudzbina().getStavkeNarudzbine().add(stavka);
        System.out.println("KontrolerUnesiArtikle: Artikal je dodat...");
        panel.getLabelGreska().setIcon(new ImageIcon("src/slike/ok1.png"));
        panel.getLabelGreska().setText(" Artikal br. " + redniBrojStavke + " je uspesno dodat");
        panel.getLabelGreska().setVisible(true);
        panel.getSpinnerKolicina().setValue("1");
        KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleStavki();
    }

    public Artikal vratiIzabraniArtikalIzTabeleArtikala() {
        return ((TableModelArtikli) panel.getTableArtikli().getModel()).vratiSelektovaniArtikal(panel.getTableArtikli().getSelectedRow());
    }

    public void potvrdiUnos() {
        panel.getLabelGreska().setVisible(false);
        panel.getLabelGreskaKolicina().setVisible(false);
        if (sto.getNarudzbina().getStavkeNarudzbine().isEmpty()) {
            new OkDialog(null, "Greska", "Ne mozete sacuvati praznu narudzbinu", true, OkDialog.ERROR);
            return;
        }
        int indexStola = KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().indexOf(sto);
        KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().get(indexStola).getNarudzbina().setStavkeNarudzbine(sto.getNarudzbina().getStavkeNarudzbine());
        KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleNarudzbina(KontrolerKIPanelGlavniKonobar.getInstance().filtrirajStolovePoNarudzbini(KontrolerKIPanelGlavniKonobar.getInstance().getStolovi()));
        panel.getRoditelj().dispose();
    }

    public void otkaziUnos() {
        if (sto.getNarudzbina().isPotvrdjena()) {
            panel.getRoditelj().dispose();
            return;
        }
        panel.getLabelGreska().setVisible(false);
        panel.getLabelGreskaKolicina().setVisible(false);
        sto.setNarudzbina(new Narudzbina());
        panel.getRoditelj().dispose();
        int indexStola = KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().indexOf(sto);
        KontrolerKIPanelGlavniKonobar.getInstance().getStolovi().get(indexStola).getNarudzbina().setStavkeNarudzbine(sto.getNarudzbina().getStavkeNarudzbine());
        KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleNarudzbina(KontrolerKIPanelGlavniKonobar.getInstance().filtrirajStolovePoNarudzbini(KontrolerKIPanelGlavniKonobar.getInstance().getStolovi()));
        KontrolerKIPanelGlavniKonobar.getInstance().postaviModelTabeleStavki();
    }
}
