/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.rezervacije;

import javax.swing.border.EmptyBorder;
import view.panels.rezervacije.kontrolerki.KontrolerKIRezervacijeStola;
import view.panels.rezervacije.kontrolerki.KontrolerKIlobodniStolovi;

/**
 *
 * @author Nemanja
 */
public class PanelSlobodniStolovi extends javax.swing.JPanel {

    /**
     * Creates new form PanelSlobodniStolovi
     */
    public PanelSlobodniStolovi() {
        initComponents();
        KontrolerKIlobodniStolovi.getInstance().setTabelaStolova(tableSlobodniStolovi);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableSlobodniStolovi = new javax.swing.JTable();

        setBackground(new java.awt.Color(153, 153, 153));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Slobodni Stolovi", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Gill Sans MT", 1, 14), new java.awt.Color(68, 68, 68))); // NOI18N
        setForeground(new java.awt.Color(51, 51, 51));

        tableSlobodniStolovi.setBackground(new java.awt.Color(136, 136, 136));
        tableSlobodniStolovi.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        tableSlobodniStolovi.setForeground(new java.awt.Color(51, 51, 51));
        tableSlobodniStolovi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableSlobodniStolovi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSlobodniStoloviMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableSlobodniStolovi);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableSlobodniStoloviMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSlobodniStoloviMouseClicked
        KontrolerKIRezervacijeStola.getInstance().postaviModel(KontrolerKIlobodniStolovi.getInstance().vratiSelectovaniObjekat());
    }//GEN-LAST:event_tableSlobodniStoloviMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableSlobodniStolovi;
    // End of variables declaration//GEN-END:variables

    private void inicijalizacijaIzgleda() {
        this.setBorder(new EmptyBorder(10, 10, 10, 10));
    }
    
    
    
}
