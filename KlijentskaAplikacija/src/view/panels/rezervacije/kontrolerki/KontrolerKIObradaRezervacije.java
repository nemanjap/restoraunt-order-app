/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.rezervacije.kontrolerki;

import com.toedter.calendar.JDateChooser;
import domain.Rezervacija;
import domain.Sto;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import remote.Komunikacija;
import utils.Capitalization;
import view.dialogs.OkDialog;
import view.models.TableModelRezervacije;
import view.panels.rezervacije.PanelObrada;
import view.panels.rezervacije.PanelUnos;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIObradaRezervacije {

    private PanelObrada panel;

    public PanelObrada getPanel() {
        return panel;
    }

    public void setPanel(PanelObrada panel) {
        this.panel = panel;
    }

    private KontrolerKIObradaRezervacije() {
    }
    
    private static KontrolerKIObradaRezervacije instance;

    public static KontrolerKIObradaRezervacije getInstance() {
        if (instance == null) {
            instance = new KontrolerKIObradaRezervacije();
        }
        return instance;
    }

    public void pretraziRezervacije() {
        JTextField poljeZaPretragu = panel.getTextFieldPoljeZaPretragu();
        skloniSveLabeleZaGreske();
        if (poljeZaPretragu.getText().isEmpty()) {
            System.out.println("KontrolerObradaRezervacije: Proces pretrazivanja zavrsen usled praznih polja...");
            panel.getLabelGreska().setText(" Niste uneli kriterijum za pretragu");
            panel.getLabelGreska().setVisible(true);
            poljeZaPretragu.requestFocus();
            postaviModelTabele(new ArrayList<Rezervacija>());
            return;
        }

        String kriterijumPretrage = Capitalization.capitalizeFully(poljeZaPretragu.getText(), null);
        ArrayList<Rezervacija> rezervacije = new ArrayList<>();
        for (Sto sto : KontrolerKIGlavniRezervacije.getInstance().getStolovi()) {
            for (Rezervacija rez : sto.getRezervacije()) {
                if (rez.getNosilacRezervacije().startsWith(kriterijumPretrage)) {
                    rezervacije.add(rez);
                }
            }
        }
        postaviModelTabele(rezervacije);
        poljeZaPretragu.requestFocus();
    }

    public void postaviModelTabele(ArrayList<Rezervacija> rezervacije) {
        if (rezervacije != null) {
            panel.getTableRezervacije().setModel(new TableModelRezervacije(rezervacije));
        }
    }

    public Rezervacija vratiSelectovaniObjekat() {
        int index = panel.getTableRezervacije().getSelectedRow();
        if (index != -1) {
            return ((TableModelRezervacije) panel.getTableRezervacije().getModel()).vratiRezervaciju(index);
        }
        return null;
    }

    public void izmeniSelectovaniObjekat() {
        skloniSveLabeleZaGreske();
        Rezervacija rezervacija = vratiSelectovaniObjekat();
        if(rezervacija == null){
            panel.getLabelGreska().setText(" Niste selektovali u tabeli rezervaciju koju zelite da izmenite");
            panel.getLabelGreska().setVisible(true);
            return;
        }
        Frame frame = null;
        KontrolerKIGlavniRezervacije.getInstance().ucitajStolove();
        JDialog dialog = new JDialog(frame, "Izmena Rezervacije", true);
        PanelUnos panel = new PanelUnos(dialog);
        KontrolerKIUnosRezervacije.getInstance().postaviVrednostiPolja(rezervacija);
        KontrolerKIUnosRezervacije.getInstance().postaviModUnos(false);
        KontrolerKIlobodniStolovi.getInstance().postaviModelTabele(KontrolerKIGlavniRezervacije.getInstance().getStolovi());
        dialog.add(panel);
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    public void obrisiRezervaciju() {
        skloniSveLabeleZaGreske();
        try {
            Rezervacija rezervacija = vratiSelectovaniObjekat();
            if (rezervacija != null) {
                Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.OBRISI_REZERVACIJU, rezervacija);
                new OkDialog(null, "Uspeh", "Rezervacija je uspesno obrisana", true, OkDialog.SUCCESS);
                int indexStola = KontrolerKIGlavniRezervacije.getInstance().getStolovi().indexOf(rezervacija.getSto());
                KontrolerKIGlavniRezervacije.getInstance().getStolovi().get(indexStola).getRezervacije().remove(rezervacija);
                resetujSvaPolja();
            } else {
                panel.getLabelGreska().setText(" Niste selektovali u tabeli rezervaciju koju zelite da obrisete");
                panel.getLabelGreska().setVisible(true);
            }
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIObradaRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void izmeniRezervaciju(Sto sto, JTextField nosilacRezField, JDateChooser dateChooser, JSpinner sati, JSpinner minuti) {
       KontrolerKIUnosRezervacije.getInstance().izmeniRezervaciju(sto, nosilacRezField, dateChooser, sati, minuti);
    }

    public void resetujSvaPolja() {
        pretraziRezervacije();
        panel.getTextFieldPoljeZaPretragu().requestFocus();
    }
    
        
    public void skloniSveLabeleZaGreske(){
        panel.getLabelGreska().setVisible(false);
    }
    
}
