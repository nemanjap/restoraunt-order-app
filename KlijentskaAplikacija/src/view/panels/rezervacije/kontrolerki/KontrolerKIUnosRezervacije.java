/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.rezervacije.kontrolerki;

import com.toedter.calendar.JDateChooser;
import domain.Rezervacija;
import domain.Sto;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import remote.Komunikacija;
import utils.Capitalization;
import view.dialogs.OkDialog;
import view.panels.rezervacije.PanelUnosRezervacija;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIUnosRezervacije {

    private PanelUnosRezervacija panel;

    public PanelUnosRezervacija getPanel() {
        return panel;
    }

    public void setPanel(PanelUnosRezervacija panel) {
        this.panel = panel;
    }

    private KontrolerKIUnosRezervacije() {
    }
    private static KontrolerKIUnosRezervacije instance;

    public static KontrolerKIUnosRezervacije getInstance() {
        if (instance == null) {
            instance = new KontrolerKIUnosRezervacije();
        }
        return instance;
    }

    public void zapamtiRezervaciju(Sto sto, JTextField nosilacRezField, JDateChooser dateChooser, JSpinner sati, JSpinner minuti) {
        skloniSveLabeleZaGreske();
        try {
            if (sto == null || nosilacRezField.getText().isEmpty() || ((JTextField) dateChooser.getDateEditor().getUiComponent()).getText().isEmpty() || sati.getValue().toString().equals("0")) {
                System.out.println("KontrolerUnosRezervacije: Cuvanje prekinuto usled praznih polja...");
                if (((JTextField) dateChooser.getDateEditor().getUiComponent()).getText().isEmpty()) {
                    System.out.println("KontrolerUnosRezervacije: Datum je prazan");
                    panel.getLabelDatumPrazan().setText(" Morate izabrati datum rezervacije");
                    panel.getLabelDatumPrazan().setVisible(true);
                    return;
                }
                if (sati.getValue().toString().equals("0")) {
                    System.out.println("KontrolerUnosRezervacije: Vreme nije izabrano");
                    panel.getLabelVremePrazno().setText(" Morate izabrati vreme rezervacije");
                    panel.getLabelVremePrazno().setVisible(true);
                    ((JSpinner.DefaultEditor) sati.getEditor()).getTextField().requestFocus();
                    return;
                }
                if (nosilacRezField.getText().isEmpty()) {
                    System.out.println("KontrolerUnosRezervacije: Nosilac rezervacije je prazan");
                    panel.getLabelNosilacRezervacijePrazno().setText(" Morate naglasiti na koga glasi rezervacija");
                    panel.getLabelNosilacRezervacijePrazno().setVisible(true);
                    nosilacRezField.requestFocus();
                    return;
                }
                if (sto == null) {
                    System.out.println("KontrolerUnosRezervacije: Sto nije izabran je prazan");
                    panel.getLabelBrMestaPrazno().setText(" Morate izabrati sto iz tabele pored ==>>");
                    panel.getLabelBrMestaPrazno().setVisible(true);
                    panel.getTextFieldBrojMesta().requestFocus();
                    KontrolerKIlobodniStolovi.getInstance().getTabelaStolova().setRowSelectionInterval(0, 0);
                    return;
                }

                return;
            }
            SimpleDateFormat dfDatum = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat dfFullDate = new SimpleDateFormat("MM/dd/yyyy HH:mm");

            String datumString = dfDatum.format(dateChooser.getDate());
            System.out.println(datumString);
            String satiString = sati.getValue().toString();
            String minutiString = minuti.getValue().toString();

            Date fullDatum = dfFullDate.parse(datumString + " " + satiString + ":" + minutiString);
            System.out.println(fullDatum);

            String nosilacRezervacije = Capitalization.capitalizeFully(nosilacRezField.getText(), null);

            Rezervacija rezervacija = new Rezervacija(sto, nosilacRezervacije, fullDatum);
            
            Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.ZAPAMTI_REZERVACIJU, rezervacija);
            new OkDialog(null, "Uspeh", "Sistem je uspesno sacuvao rezervaciju\n\nIme:" + nosilacRezervacije + "\n\nVreme: " + dfFullDate.format(fullDatum) + "\n\nSto: " + sto, true, OkDialog.SUCCESS);
            int indexStola = KontrolerKIGlavniRezervacije.getInstance().getStolovi().indexOf(sto);
            KontrolerKIGlavniRezervacije.getInstance().getStolovi().get(indexStola).getRezervacije().add(rezervacija);
            resetujSvaPolja();


        } catch (ParseException ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void izmeniRezervaciju(Sto sto, JTextField nosilacRezField, JDateChooser dateChooser, JSpinner sati, JSpinner minuti) {
        skloniSveLabeleZaGreske();
        try {
            if (sto == null || nosilacRezField.getText().isEmpty() || ((JTextField) dateChooser.getDateEditor().getUiComponent()).getText().isEmpty() || sati.getValue().toString().equals("0")) {
                System.out.println("KontrolerUnosRezervacije: Cuvanje prekinuto usled praznih polja...");
                if (((JTextField) dateChooser.getDateEditor().getUiComponent()).getText().isEmpty()) {
                    System.out.println("KontrolerUnosRezervacije: Datum je prazan");
                    panel.getLabelDatumPrazan().setText(" Morate izabrati datum rezervacije");
                    panel.getLabelDatumPrazan().setVisible(true);
                    return;
                }
                if (sati.getValue().toString().equals("0")) {
                    System.out.println("KontrolerUnosRezervacije: Vreme nije izabrano");
                    panel.getLabelVremePrazno().setText(" Morate izabrati vreme rezervacije");
                    ((JSpinner.DefaultEditor) sati.getEditor()).getTextField().requestFocus();
                    panel.getLabelVremePrazno().setVisible(true);
                    return;
                }
                if (nosilacRezField.getText().isEmpty()) {
                    System.out.println("KontrolerUnosRezervacije: Nosilac rezervacije je prazan");
                    panel.getLabelNosilacRezervacijePrazno().setText(" Morate naglasiti na koga glasi rezervacija");
                    panel.getLabelNosilacRezervacijePrazno().setVisible(true);
                    nosilacRezField.requestFocus();
                    return;
                }
                if (sto == null) {
                    System.out.println("KontrolerUnosRezervacije: Sto nije izabran je prazan");
                    panel.getLabelBrMestaPrazno().setText(" Morate izabrati sto iz tabele pored ==>>");
                    panel.getLabelBrMestaPrazno().setVisible(true);
                    panel.getTextFieldBrojMesta().requestFocus();
                    return;
                }
                return;
            }
            SimpleDateFormat dfDatum = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat dfFullDate = new SimpleDateFormat("MM/dd/yyyy HH:mm");

            String datumString = dfDatum.format(dateChooser.getDate());
            System.out.println(datumString);
            String satiString = sati.getValue().toString();
            String minutiString = minuti.getValue().toString();

            Date fullDatum = dfFullDate.parse(datumString + " " + satiString + ":" + minutiString);
            System.out.println(fullDatum);

            String nosilacRezervacije = Capitalization.capitalizeFully(nosilacRezField.getText(), null);

            Rezervacija rezervacija = new Rezervacija(KontrolerKIObradaRezervacije.getInstance().vratiSelectovaniObjekat().getRezervacijaId(), sto, nosilacRezervacije, fullDatum);

            Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.IZMENI_REZERVACIJU, rezervacija);

            KontrolerKIGlavniRezervacije.getInstance().ucitajStolove();

            new OkDialog(null, "Uspeh", "Rezervacija je uspesno izmenjena", true, OkDialog.SUCCESS);

            KontrolerKIUnosRezervacije.getInstance().zatvoriProzor();

            KontrolerKIObradaRezervacije.getInstance().resetujSvaPolja();

        } catch (ParseException ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIUnosRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void resetujSvaPolja() {
        panel.getDateChooserDatum().setDate(new Date());
        panel.getTextFieldBrojMesta().setText(null);
        panel.getSpinnerSati().setValue("0");
        panel.getSpinnerMinuti().setValue("0");
        panel.getTextFieldNosilacRezervacije().setText(null);
        KontrolerKIlobodniStolovi.getInstance().postaviModelTabele(KontrolerKIGlavniRezervacije.getInstance().getStolovi());
        KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());

    }

    public void postaviVrednostiPolja(Rezervacija rezervacija) {
        System.out.println(rezervacija);
        SimpleDateFormat dfSati = new SimpleDateFormat("HH");
        SimpleDateFormat dfMinuti = new SimpleDateFormat("mm");

        String satiS = dfSati.format(rezervacija.getDatum());
        int satiI = Integer.parseInt(satiS);
        String minutiS = dfMinuti.format(rezervacija.getDatum());
        int minutiI = Integer.parseInt(minutiS);

        panel.getDateChooserDatum().setDate(rezervacija.getDatum());
        panel.getTextFieldBrojMesta().setText(Integer.toString(rezervacija.getSto().getBrojMesta()));
        panel.getSpinnerSati().setValue(Integer.toString(satiI));
        panel.getSpinnerMinuti().setValue(Integer.toString(minutiI));
        panel.getTextFieldNosilacRezervacije().setText(rezervacija.getNosilacRezervacije());
    }

    public void postaviModUnos(boolean isUnos) {
        panel.getButtonPotvrdi().setVisible(isUnos);
        panel.getButtonSacuvajIzmene().setVisible(!isUnos);
    }

    public void zatvoriProzor() {
        panel.getRoditelj().dispose();
    }

    public void skloniSveLabeleZaGreske() {
        panel.getLabelDatumPrazan().setVisible(false);
        panel.getLabelBrMestaPrazno().setVisible(false);
        panel.getLabelVremePrazno().setVisible(false);
        panel.getLabelNosilacRezervacijePrazno().setVisible(false);
    }
}
