/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.rezervacije.kontrolerki;

import com.toedter.calendar.JDateChooser;
import domain.Rezervacija;
import domain.Sto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import view.models.TableModelSlobodniStolovi;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIlobodniStolovi {

    private JTable tabelaStolova;
    private ArrayList<Sto> azuriranaLista;

    public JTable getTabelaStolova() {
        return tabelaStolova;
    }

    public void setTabelaStolova(JTable tabelaStolova) {
        this.tabelaStolova = tabelaStolova;
    }

    private KontrolerKIlobodniStolovi() {
    }
    private static KontrolerKIlobodniStolovi instance;

    public static KontrolerKIlobodniStolovi getInstance() {
        if (instance == null) {
            instance = new KontrolerKIlobodniStolovi();
        }
        return instance;
    }

    public void postaviModelTabele(ArrayList<Sto> stolovi) {
        tabelaStolova.setModel(new TableModelSlobodniStolovi(stolovi));
        azuriranaLista = stolovi;
        KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
    }

    public void postaviModelTabelePremaKriterijumima(JDateChooser dateChooser, JTextField brojMestaField, JSpinner sati, JSpinner minuti) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        ArrayList<Sto> filtriraniStlovi = null;
        if(!((JTextField)dateChooser.getDateEditor().getUiComponent()).getText().isEmpty() && !brojMestaField.getText().isEmpty() && !sati.getValue().toString().equals("0")){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter sve...");
            Date vreme = df.parse(sati.getValue().toString()+":"+minuti.getValue().toString());
            filtriraniStlovi = filterVreme(vreme, filterBrStolova(Integer.parseInt(brojMestaField.getText()), filterDatum(dateChooser.getDate(), KontrolerKIGlavniRezervacije.getInstance().getStolovi())));
            tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
            KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }else if(!((JTextField)dateChooser.getDateEditor().getUiComponent()).getText().isEmpty() && !brojMestaField.getText().isEmpty()){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter date+brojmesta...");
            filtriraniStlovi = filterBrStolova(Integer.parseInt(brojMestaField.getText()), filterDatum(dateChooser.getDate(), KontrolerKIGlavniRezervacije.getInstance().getStolovi()));
            tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
            KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }else if(!((JTextField)dateChooser.getDateEditor().getUiComponent()).getText().isEmpty() && !sati.getValue().toString().equals("0")){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter date+vreme");
            Date vreme = df.parse(sati.getValue().toString()+":"+minuti.getValue().toString());
            filtriraniStlovi = filterVreme(vreme, filterDatum(dateChooser.getDate(), KontrolerKIGlavniRezervacije.getInstance().getStolovi()));
            tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
            KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }else if(!sati.getValue().toString().equals("0") && !brojMestaField.getText().isEmpty()){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter vreme+mesta...");
            Date vreme = df.parse(sati.getValue().toString()+":"+minuti.getValue().toString());
            filtriraniStlovi = filterVreme(vreme, filterBrStolova(Integer.parseInt(brojMestaField.getText()), KontrolerKIGlavniRezervacije.getInstance().getStolovi()));
            tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
            KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }else if(!sati.getValue().toString().equals("0")){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter sati...");
             Date vreme = df.parse(sati.getValue().toString()+":"+minuti.getValue().toString());
             filtriraniStlovi = filterVreme(vreme, KontrolerKIGlavniRezervacije.getInstance().getStolovi());
             tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
             KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }else if(!((JTextField)dateChooser.getDateEditor().getUiComponent()).getText().isEmpty()){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter datum...");
            filtriraniStlovi = filterDatum(dateChooser.getDate(), KontrolerKIGlavniRezervacije.getInstance().getStolovi());
            tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
            KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }else if(!brojMestaField.getText().isEmpty()){
            System.out.println("KontrolerSlobodniStolovi: Primenjujem filter broj mesta...");
            filtriraniStlovi = filterBrStolova(Integer.parseInt(brojMestaField.getText()), KontrolerKIGlavniRezervacije.getInstance().getStolovi());
            tabelaStolova.setModel(new TableModelSlobodniStolovi(filtriraniStlovi));
            KontrolerKIRezervacijeStola.getInstance().postaviModel(new Sto());
        }
    }

    private ArrayList<Sto> filterDatum(Date datum, ArrayList<Sto> stoloviZaFiltriranje) {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String formatiranDatum = df.format(datum);
        ArrayList<Sto> stoloviFiltrirani = new ArrayList<>();
        for (Sto sto : stoloviZaFiltriranje) {
            boolean slobodan = true;
            for (Rezervacija rez : sto.getRezervacije()) {
                String datumZaPoredjenje = df.format(rez.getDatum());
                if (datumZaPoredjenje.equals(formatiranDatum)) {
                    System.out.println(rez.getDatum());
                    System.out.println(datum);
                    slobodan = false;
                    break;
                }
            }
            if (slobodan) {
                stoloviFiltrirani.add(sto);
            }
        }
        return stoloviFiltrirani;
    }

    private ArrayList<Sto> filterBrStolova(int brMesta, ArrayList<Sto> stoloviZaFiltriranje) {
        ArrayList<Sto> stoloviFiltrirani = new ArrayList<>();
        for (Sto sto : stoloviZaFiltriranje) {
            if(sto.getBrojMesta() >= brMesta)
                stoloviFiltrirani.add(sto);
        }
        return stoloviFiltrirani;
    }
    
    private ArrayList<Sto> filterVreme(Date vreme, ArrayList<Sto> stoloviZaFiltriranje){
        ArrayList<Sto> filtriraniStolovi = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        for (Sto sto : stoloviZaFiltriranje) {
            boolean slobodan = true;
            for (Rezervacija rez : sto.getRezervacije()) {
                if(df.format(rez.getDatum()).equals(df.format(vreme))){
                    slobodan = false;
                    break;
                }
            }
            if(slobodan){
                filtriraniStolovi.add(sto);
            }
        }
        return filtriraniStolovi;
    }
    
    public Sto vratiSelectovaniObjekat(){
        if(tabelaStolova.getSelectedRow() != -1){
            return ((TableModelSlobodniStolovi)tabelaStolova.getModel()).vratiObjekat(tabelaStolova.getSelectedRow());        
        }
        return null;
    }
    
    public boolean isObjekatSelectovan(){
        if(tabelaStolova.getSelectedRow() != -1)
            return true;
        return false;
    }
}
