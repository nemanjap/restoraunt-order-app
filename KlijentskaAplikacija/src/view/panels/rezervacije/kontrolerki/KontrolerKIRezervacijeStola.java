/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.rezervacije.kontrolerki;

import domain.Sto;
import javax.swing.JTable;
import view.models.TableModelRezervacijeStola;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIRezervacijeStola {
    
    private JTable tabelaRezervacija;

    public JTable getTabelaRezervacija() {
        return tabelaRezervacija;
    }

    public void setTabelaRezervacija(JTable tabelaRezervacija) {
        this.tabelaRezervacija = tabelaRezervacija;
    }
    
    private KontrolerKIRezervacijeStola(){}
    
    private static KontrolerKIRezervacijeStola instance;
    
    public static KontrolerKIRezervacijeStola getInstance(){
        if(instance == null)
            instance = new KontrolerKIRezervacijeStola();
        return instance;
    }
    
    public void postaviModel(Sto sto){
        if(sto != null)
            tabelaRezervacija.setModel(new TableModelRezervacijeStola(sto));
    }
    
}
