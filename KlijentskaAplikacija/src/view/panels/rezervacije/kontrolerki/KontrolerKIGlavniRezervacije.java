/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.panels.rezervacije.kontrolerki;

import domain.Sto;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import remote.Komunikacija;

/**
 *
 * @author Nemanja
 */
public class KontrolerKIGlavniRezervacije {
    
    private ArrayList<Sto> stolovi;
    
    private KontrolerKIGlavniRezervacije(){
        
    }

    public ArrayList<Sto> getStolovi() {
        return stolovi;
    }

    public void setStolovi(ArrayList<Sto> stolovi) {
        this.stolovi = stolovi;
    }
    
    private static KontrolerKIGlavniRezervacije instance;
    
    public static KontrolerKIGlavniRezervacije getInstance(){
        if(instance == null)
            instance = new KontrolerKIGlavniRezervacije();
        return instance;
    }
    
    public void ucitajStolove(){
        try {
            stolovi = (ArrayList<Sto>) Komunikacija.getInstance().posaljiPoruku(konstante.Konstante.VRATI_SVE_STOLOVE, new Sto()).getRezultat();
            System.out.println("KontrolerGlavniRezervacije: Stolovi su ucitani...");
        } catch (UnknownHostException ex) {
            Logger.getLogger(KontrolerKIGlavniRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KontrolerKIGlavniRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KontrolerKIGlavniRezervacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
