/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.dialogs;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Nemanja
 */
public class OkDialog extends JDialog {

    private JButton okButton = new JButton("OK");
    private Color color = new Color(136, 136, 136, 255);
    ;
    private JLabel icon;
    private JLabel messageLabel = new JLabel();
    private JTextArea messageTextArea = new JTextArea();
    public static final int SUCCESS = 1;
    public static final int ERROR = 2;

    private OkDialog() {
        color = new Color(136, 136, 136, 255);
    }

    public OkDialog(Dialog dialog, String naslov, String poruka, boolean bln, int tipPoruke) {
        super(dialog, naslov, bln);
        messageTextArea.setText(poruka);
        if (tipPoruke == SUCCESS) {
            icon = new JLabel(new ImageIcon("src/slike/success.png"));
        } else {
            icon = new JLabel(new ImageIcon("src/slike/error.png"));
        }
        initializeComponents();
    }

    private void initializeComponents() {
        okButton.setFont(new Font("Gill Sans MT", Font.BOLD, 14));
        this.getContentPane().setBackground(color);
        okButton.setBackground(color);
        this.setForeground(new Color(51, 51, 51));
        okButton.setForeground(new Color(51, 51, 51));
        icon.setBackground(color);
        messageTextArea.setEditable(false);
        messageTextArea.setFont(new Font("Gill Sans MT", Font.BOLD, 14));
        messageTextArea.setBackground(color);
        messageTextArea.setForeground(new Color(51, 51, 51));
        messageTextArea.setBorder(new EmptyBorder(10, 10, 10, 10));
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        this.add(icon, c);

        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        this.add(messageTextArea, c);

        c.fill = GridBagConstraints.VERTICAL;
        c.ipady = 0;
        c.weighty = 1.0;
        c.anchor = GridBagConstraints.PAGE_END;
        c.insets = new Insets(10, 10, 10, 10);
        c.gridx = 1;
        c.gridwidth = 2;
        c.gridy = 2;
        this.add(okButton, c);
        
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
