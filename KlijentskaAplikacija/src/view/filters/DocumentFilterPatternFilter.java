/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view.filters;

import java.awt.Toolkit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 *
 * @author Nemanja
 */
public class DocumentFilterPatternFilter extends DocumentFilter {

    private Pattern pattern;

    /**
     *
     * @param pat regular expression against witch the entry will be checked
     */
    public DocumentFilterPatternFilter(String pat) {
        pattern = Pattern.compile(pat);
    }

    @Override
    public void insertString(FilterBypass fb, int i, String string, AttributeSet as) throws BadLocationException {
        String newStr = fb.getDocument().getText(0, fb.getDocument().getLength()) + string;
        Matcher m = pattern.matcher(newStr);
        if (m.matches()) {
            super.insertString(fb, i, string, as);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    @Override
    public void replace(FilterBypass fb, int i, int i1, String string, AttributeSet as) throws BadLocationException {
        if (i1 > 0) {
            fb.remove(i, i1);
        }
        insertString(fb, i, string, as);
    }
}
