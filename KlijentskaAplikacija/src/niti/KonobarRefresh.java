/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niti;

import java.util.logging.Level;
import java.util.logging.Logger;
import remote.Komunikacija;
import view.panels.konobar.kontrolerki.KontrolerKIPanelGlavniKonobar;

/**
 *
 * @author Nemanja
 */
public class KonobarRefresh extends Thread{
    boolean pokrenuta = false;
    @Override
    public void run() {
        pokrenuta = true;
        while(pokrenuta){
            
            try {
                KontrolerKIPanelGlavniKonobar.getInstance().osveziListuNarudzbina();
                this.sleep(50000);
                System.out.println("..........................................................");
            } catch (InterruptedException ex) {
                Logger.getLogger(KonobarRefresh.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
}
